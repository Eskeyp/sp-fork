Install

virtualenv -p python3 myenv
pip install django gunicorn psycopg2 psycopg2-binary djangorestframework markdown django-filter requests bs4 python-rucaptcha redis celery Pillow

BD
sudo apt-get install libpq-dev python-dev
sudo apt-get install postgresql postgresql-contrib

createdb socialprom
createuser main -P

password: 12345

psql

GRANT ALL PRIVILEGES ON DATABASE socialprom TO main;
