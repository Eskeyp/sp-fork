from django.db import models
from socialprom.accounts.models import User
# Create your models here.
from django.utils.translation import ugettext_lazy as _


class SocialAccount(models.Model):
    class Meta:
        abstract = True

    class Status:
        OFF = "0"
        NOAUTH = "1"
        ACTIVE = "2"
        ERROR = "3"
        NORECOMENTED = "4"
        LIMITMAX = "5"

        choices = (
            (OFF, 'Ожидает включения'),
            (NOAUTH, 'Ожидает авторизации'),
            (ACTIVE, 'Активирован'),
            (ERROR, 'Ошибка авторизации'),
            (NORECOMENTED, 'Нет рекомендованных друзей'),
            (LIMITMAX, 'Достигнут лимит друзей/день'),
        )

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    login = models.CharField(_('login'), max_length=100)
    password = models.CharField(_('password'), max_length=100)
    proxy = models.CharField(_('proxy'), max_length=100, blank=True, null=True)
    avatar = models.ImageField(_('account_avatar'), null=True, upload_to='account_avatar')
    status = models.CharField(_('status'),
        max_length=1,
        choices=Status.choices,
        default=Status.OFF
    )
