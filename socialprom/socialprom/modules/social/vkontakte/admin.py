from django.contrib import admin
from socialprom.modules.social.vkontakte.models import VkAccount, TaskAddToFriends, VkAccountFriendsList, VkAccountLogsList
from django.db.models import Count

class VkAccountAdmin(admin.ModelAdmin):
    model = VkAccount
    list_display =('login','status','proxy','get_follow','get_unfollow','get_deleted')

    def get_follow(self, obj):
        return VkAccountFriendsList.objects.filter(account_id=obj.id).count()

    get_follow.short_description = 'follow'

    def get_unfollow(self, obj):
        return VkAccountFriendsList.objects.filter(account_id=obj.id, unfollow=True, deleted=False).count()

    get_unfollow.short_description = 'unfollow'

    def get_deleted(self, obj):
        return VkAccountFriendsList.objects.filter(account_id=obj.id, deleted=True, unfollow=False).count()

    get_deleted.short_description = 'deleted'



admin.site.register(VkAccount, VkAccountAdmin)
