from socialprom.modules.social.vkontakte.lib import Vk
from socialprom.modules.social.vkontakte.models import VkAccount, TaskAddToFriends, VkAccountFriendsList, VkAccountLogsList
from socialprom.modules.social.limits.models import VkAccountLimits
from celery import shared_task
from socialprom.celery import app
from celery import task
from datetime import datetime, timedelta
from django.db.models import Count
import random
import math

@app.task()
def add_to_friends(account_id, login, password, phone, proxy, limits):
    if proxy:
        user_proxy = proxy
    else:
        user_proxy = ""
    obj = Vk(login, password, follow_per_day=2, account_id=account_id, proxy=user_proxy)
    status, message = obj.login()
    if status:
        obj.auto_follow()
        obj.logout()
    else:
        account = VkAccount.objects.get(id=account_id)
        account.status = VkAccount.Status.ERROR
        account.save()
        VkAccountLogsList.objects.create(
            account_id=account_id,
            message=message,
            status=1
            )

@shared_task()
def get_active_user_tasks():
    active_tasks = TaskAddToFriends.objects.filter(account__status=VkAccount.Status.ACTIVE, active=True).values('account')
    for account_id in active_tasks:
        account = VkAccount.objects.filter(id=account_id['account']).values(
            'id', 'login', 'password', 'phone', 'proxy'
            ).first()
        add_to_friends.delay(
            account['id'],
            account['login'],
            account['password'],
            account['phone'],
            account['proxy'],
            5,
            )

@app.task()
def delete_of_friends(action, list_id, list_href, account_id, login, password, phone, proxy, is_delete=False):
    if proxy:
        user_proxy = proxy
    else:
        user_proxy = ""

    obj = Vk(login, password, follow_per_day=len(list_id), account_id=account_id, proxy=user_proxy)

    status, message = obj.login()

    if status:
        if action == '3_days':
            obj.auto_delete_3_days(list_id, list_href, is_delete=is_delete)
            obj.logout()
        elif action == '10_days':
            obj.auto_unfollow_10_days(list_id, list_href)
            obj.logout()

    else:
        account = VkAccount.objects.get(id=account_id)
        account.status = VkAccount.Status.ERROR
        account.save()
        VkAccountLogsList.objects.create(
            account_id=account_id,
            message=message,
            status=1
            )
#VkAccountFriendsList.objects.raw('SELECT id, account_id, account_href FROM vkontakte_vkaccountfriendslist WHERE #datetime_3_days__lt >=%s GROUP BY account_id',[datetime.now()])
@shared_task()
def get_friends_list_3_days():
    """
    Получить ссылки на пользователей у которых прошло 3 дня со дня добавления
    Передать в функцию проверки. Если пользователь добавил в друзья,
    то удалить его из друзей и удалить из этого списка, если не добавил в друзья,
    то ничего не делать.
    """
    accounts = list(VkAccountFriendsList.objects.values('account').annotate(count=Count('account')))
    for _id in accounts:
        account = VkAccount.objects.filter(id=_id['account']).values(
            'id', 'login', 'password', 'phone', 'proxy'
            ).first()
        _list = VkAccountFriendsList.objects.filter(
            deleted=False,
            unfollow=False,
            add_to_friend=False,
            account_id=_id['account'],
            datetime_3_days__lt=datetime.now()
            ).values('id','account','account_href')[:10]
        href_list = []
        friends_list_id = []
        for link in _list:
            href_list.append(link['account_href'])
            friends_list_id.append(link['id'])
        if len(href_list) > 0:
            action='3_days'
            delete_of_friends.delay(
                action,
                friends_list_id,
                href_list,
                account['id'],
                account['login'],
                account['password'],
                account['phone'],
                account['proxy'],
                )

@shared_task()
def get_friends_list_to_delete():
    """
    Получить ссылки на пользователей у которых прошло 3 дня со дня добавления
    Передать в функцию проверки. Если пользователь добавил в друзья,
    то удалить его из друзей и удалить из этого списка, если не добавил в друзья,
    то ничего не делать.
    """
    accounts = list(VkAccountFriendsList.objects.values('account').annotate(count=Count('account')))
    for _id in accounts:
        account = VkAccount.objects.filter(id=_id['account']).values(
            'id', 'login', 'password', 'phone', 'proxy'
            ).first()
        _list = VkAccountFriendsList.objects.filter(
            deleted=False,
            unfollow=False,
            add_to_friend=True,
            account_id=_id['account'],
            datetime_3_days__lt=datetime.now()
            ).values('id','account','account_href')[:10]
        href_list = []
        friends_list_id = []
        for link in _list:
            href_list.append(link['account_href'])
            friends_list_id.append(link['id'])
        if len(href_list) > 0:
            action='3_days'
            delete_of_friends.delay(
                action,
                friends_list_id,
                href_list,
                account['id'],
                account['login'],
                account['password'],
                account['phone'],
                account['proxy'],
                is_delete=True,
                )

@shared_task()
def get_friends_list_10_days():
    """
    Получить ссылки на пользователей у которых прошло 10 дней со дня добавления
    Передать в функцию проверки. Если пользователь добавил в друзья,
    то удалить его из друзей и удалить из этого списка, если не добавил в друзья,
    то отписаться от него и удалить из этого списка.
    """
    accounts = list(VkAccountFriendsList.objects.values('account').annotate(count=Count('account')))
    for _id in accounts:
        account = VkAccount.objects.filter(id=_id['account']).values(
            'id', 'login', 'password', 'phone', 'proxy'
            ).first()
        _list = VkAccountFriendsList.objects.filter(
            unfollow=False,
            deleted=False,
            add_to_friend=False,
            account_id=_id['account'],
            datetime_10_days__lt=datetime.now()
            ).values('id','account','account_href')[:10]
        href_list = []
        friends_list_id = []
        for link in _list:
            href_list.append(link['account_href'])
            friends_list_id.append(link['id'])
        if len(href_list) > 0:
            action='10_days'
            delete_of_friends.delay(
                action,
                friends_list_id,
                href_list,
                account['id'],
                account['login'],
                account['password'],
                account['phone'],
                account['proxy'],
                )
@shared_task()
def remove_status():
    VkAccount.objects.filter(
            status=VkAccount.Status.NORECOMENTED,
            datetime_wait_recomended_friends__lt=datetime.now()
            ).update(status = VkAccount.Status.ACTIVE)
    VkAccount.objects.filter(
            status=VkAccount.Status.LIMITMAX,
            datetime_limit_days_end__lt=datetime.now()
            ).update(status = VkAccount.Status.ACTIVE)
