from django.conf.urls import url
from socialprom.modules.social.vkontakte import views

app_name="vkontakte_page"

urlpatterns = [
url(r'^$', views.accounts_page, name="index"),
]
