import requests
import random
import time
from datetime import datetime, timedelta
import re
from urllib.parse import urlparse, parse_qsl, urlencode
from urllib.request import urlretrieve
from bs4 import BeautifulSoup
from python_rucaptcha import ImageCaptcha
from django.conf import settings
from socialprom.modules.social.vkontakte.models import VkAccountFriendsList, VkAccount, TaskAddToFriends
from socialprom.modules.social.vkontakte.models import VkAccountLogsList
from django.core.files import File
import os
raw_input = input
from django.core.files.temp import NamedTemporaryFile

class InteractiveMixin(object):

    def get_captcha_key(self, captcha_image_url):
        answer = ImageCaptcha.ImageCaptcha(save_format='const',
            rucaptcha_key=settings.RUCAPTCHA_KEY
            ).captcha_handler(captcha_link=captcha_image_url)

        if user_answer['errorId'] == 0:
            return True, user_answer['captchaSolve']
        elif user_answer['errorId'] == 1:
            return False, user_answer['errorBody']

    def get_auth_check_code(self, account_id):
        auth_check_code = ""
        time_to_exit = datetime.now() + timedelta(seconds=180)
        while True:
            if time_to_exit <= datetime.now():
                break
            account = VkAccount.objects.get(id=account_id)
            if account.auth_code:
                auth_check_code = account.auth_code
                break
            time.sleep(1)
        if auth_check_code:
            account.auth_code = ""
            account.save()
            return True, auth_check_code
        else:
            return False, None

class Vk(InteractiveMixin):
    LOGIN_URL = 'https://m.vk.com'
    CAPTCHA_URI = 'https://m.vk.com/captcha.php'
    VK_URL = 'https://vk.com/'
    FRIENDS_URL = 'https://vk.com/friends?act=find'
    like_counter = 0
    follow_counter = 0
    login_status = False
    next_iteration = {"Like": 0, "Follow": 0}
    user_agent = ("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/65.0.3325.181 Chrome/65.0.3325.181 Safari/537.36")

    def __init__(self, login, password,
                 like_per_day=100,
                 follow_per_day=40,
                 count_time=1,
                 account_id=None,
                 proxy=""):
        self.user_login = login
        self.user_password = password
        self.account_id = account_id
        self.bot_start = datetime.now()
        self.time_in_day = count_time * 60
        # Like
        self.like_per_day = like_per_day
        if self.like_per_day != 0:
            self.like_delay = self.time_in_day / self.like_per_day
         # Follow
        self.follow_per_day = follow_per_day
        if self.follow_per_day != 0:
            self.follow_delay = random.uniform(0, 40) # new delay in 1 hours
        # Session
        self.s = requests.Session()
        self.s.headers.update({
            'User-Agent': self.user_agent,
            })
        if proxy!="":
            proxies = {
                'http': 'http://'+proxy,
                'https': 'http://'+proxy,
                }
            self.s.proxies.update(proxies)

    def write_log(self, message, level):
        VkAccountLogsList.objects.create(
            account_id=self.account_id,
            message=message,
            status=level
            )

    def post(self, url, param):
        time.sleep(5 * random.random())
        return self.s.post(url, param)

    def get(self, url):
        time.sleep(5 * random.random())
        return self.s.get(url)

    def add_time(self, time):
        """ Make some random for next iteration"""
        return time * 0.9 + time * 0.2 * random.random()

    def get_form_action(self, html):
        form_action = re.findall(r'<form(?= ).* action="(.+)"', html)
        if form_action:
            return form_action[0]

    def get_url_query(self, url):
        parsed_url = urlparse(url)
        url_query = parse_qsl(parsed_url.query)
        url_query = dict(url_query)
        return url_query

    def is_captcha(self, url):
        if 'sid' in url:
            return True
        return False

    def mobile_captcha_is_needed(self, soup, captha_form_url):
        sid = soup.find('div', 'captcha_box').find('form').find('input',         {'name':'captcha_sid'}).get('value')
        captcha_url = self.LOGIN_URL + soup.find('div', 'captcha_box').find('form').find('img').get('src')
        captcha_form_data ={}
        captcha_form_data['captcha_sid'] = sid
        status, slove = self.get_captcha_key(captcha_url)
        if status:
            captcha_form_data['captcha_key'] = slove
            response = self.post(captha_form_url, captcha_form_data)
            self.write_log("Captha успешно разгадана", 0)
            return True
        else:
            self.write_log("Captha не разгадана: " + slove, 1)
            return False

    def auth_check_is_needed(self, html):
        auth_check_form_action = self.get_form_action(html)
        status, auth_check_code = self.get_auth_check_code(self.account_id)
        if status:
            auth_check_data = {
                'code': auth_check_code,
                '_ajax': '1',
                'remember': '1'
                }
            response = self.post(self.LOGIN_URL + auth_check_form_action, auth_check_data)
            return True
        else:
            return False

    def auth_captcha_is_needed(self, response, login_form_data):
        response_url_dict = self.get_url_query(response.url)
        captcha_form_action = self.get_form_action(response.text)

        if not captcha_form_action:
            print('Cannot find form url')
            # todo response_url_dict CAPTCHA image url
        captcha_url = '%s?s=%s&sid=%s' % (self.CAPTCHA_URI, response_url_dict['s'], response_url_dict['sid'])
        login_form_data['captcha_sid'] = response_url_dict['sid']
        login_form_data['captcha_key'] = self.get_captcha_key(captcha_url)

        response = self.post(captcha_form_action, login_form_data)

    def save_user_avatar(self):
        response = self.get(self.LOGIN_URL)
        soup = BeautifulSoup(response.text, "html.parser")

        try:
            image_url = soup.find('div', 'ip_user_link').find('img').get('src')
        except:
            return None

        account = VkAccount.objects.get(id=self.account_id)

        if image_url and not account.avatar:
            r = requests.get(image_url)
            img_temp = NamedTemporaryFile(delete = True)
            img_temp.write(r.content)
            img_temp.flush()
            img_filename = os.path.basename(urlparse(image_url).path)
            account.avatar.save(img_filename, File(img_temp), save = True)
            return account.avatar

    def logout(self):
        try:
            response = self.get(self.LOGIN_URL)
            soup = BeautifulSoup(response.text, "html.parser")
            link_logout = soup.find('ul', id="footer_menu").find('li', 'mmi_logout').find('a').get('href')
            self.get(link_logout)
            self.write_log("Logout true", 0)
        except:
            self.write_log("Logout false", 1)

    def login(self):

        login_form_data = {
            'email': self.user_login,
            'pass': self.user_password,
        }
        response = self.get(self.LOGIN_URL)
        login_form_action = self.get_form_action(response.text)

        if not login_form_action:
            pass
        response = self.post(login_form_action, login_form_data)
        response_url_query = self.get_url_query(response.url)

        if 'remixsid' in self.s.cookies or 'remixsid6' in self.s.cookies:
            self.login_status = True
            self.write_log("Авторизация прошла успешно", 0)
            return True, "Авторизация прошла успешно"

        if 'sid' in response_url_query:
            print ('auth_captcha_is_needed')
            self.auth_captcha_is_needed(response, login_form_data)

            if 'remixsid' in self.s.cookies or 'remixsid6' in self.s.cookies:
                self.login_status = True
                self.write_log("Авторизация прошла успешно", 0)
                return True, "Авторизация прошла успешно"
            self.write_log("Ошибка авторизации, неверный логин или пароль", 1)
            return False, "Ошибка авторизации, неверный логин или пароль"

        elif response_url_query.get('act') == 'authcheck':
            status = self.auth_check_is_needed(response.text)
            if status:
                self.write_log("Авторизация прошла успешно", 0)
                return True, "Авторизация прошла успешно"
            self.write_log("Ошибка авторизации, неверный код", 1)
            return False, "Ошибка авторизации, неверный логин или пароль"

        elif 'security_check' in response_url_query:
            pass
        else:
            message = 'Authorization error'
            self.login_status = False
            self.write_log("Ошибка авторизации, неверный логин или пароль", 1)
            return False, "Ошибка авторизации, неверный логин или пароль"

    def is_in_friends(self, url):
        try:
            soup = BeautifulSoup(self.get(url).content, "html.parser")
            profile_menu_list = soup.findAll('ul', 'profile_menu')
            for item in profile_menu_list[len(profile_menu_list)-1].findAll('li'):
                link = item.find('a').get('href')
                if link and \
                        'act' in self.get_url_query(link) and \
                        self.get_url_query(link)['act'] == 'decline':

                    return True, link
            return False, None
        except:
            self.write_log("is_in_friends except", 0)
            return False, 'blocked'

    def get_unfollow_link(self, url):
        try:
            soup = BeautifulSoup(self.get(url).content, "html.parser")
            for item in soup.findAll('a', 'wide_button'):
                link = item.get('href')
                if link and \
                    'act' in self.get_url_query(link) and \
                    self.get_url_query(link)['act'] == 'decline':
                    return True, link
            return False, None
        except:
            self.write_log("get_unfollow_link except", 0)
            return False, 'blocked'

    def add_to_friend(self, response):
        soup = BeautifulSoup(response.content, "html.parser")

        try:
            response = self.get(self.LOGIN_URL + soup.find('a', 'acceptFriendBtn').get('href'))
            if not "u" in self.get_url_query(response.url):
                self.write_log("Достигнуто максимальное количесвто добавлений в друзья в день", 2)
                vk_acc = VkAccount.objects.get(id=self.account_id)
                vk_acc.datetime_limit_days_end = self.datetime_limit_days_end()
                vk_acc.status = 5
                vk_acc.save()
                return 'add', False
            captha_form_url = self.LOGIN_URL + soup.find('a', 'acceptFriendBtn').get('href')
        except:
            try:
                response = self.get(self.LOGIN_URL + soup.find('div', id='mcont').find('div','op_block').find('a').get('href'))
                if not "u" in self.get_url_query(response.url):
                    self.write_log("Достигнуто максимальное количесвто добавлений в друзья в день", 2)
                    vk_acc = VkAccount.objects.get(id=self.account_id)
                    vk_acc.datetime_limit_days_end = self.datetime_limit_days_end()
                    vk_acc.status = 5
                    vk_acc.save()
                    return 'add', False
                captha_form_url = self.LOGIN_URL + soup.find('div', id='mcont').find('div','op_block').find('a').get('href')
            except:
                self.write_log("Пользователь запретил добавлять себя в друзья", 2)
                return 'add', False

        soup = BeautifulSoup(response.content, "html.parser")
        if soup.find('div', 'captcha_box'):
            print ('captcha')
            status = self.mobile_captcha_is_needed(soup, captha_form_url)
            if status:
                self.write_log("Captha успешно разгадана", 0)
                return 'captha', True
            else:
                self.write_log("Captha при добавлении в друзья не разгадана: ", 1)
                return 'captha', False
        return 'add', True

    def friends_recomented_list(self):
        self.get(self.VK_URL)
        response = self.get(self.FRIENDS_URL)
        soup = BeautifulSoup(response.content, "html.parser")
        friends_url_list = []
        for link in soup.find('div',id='friends').find('div',id='friends_list_wrap').find_all('div','friends_find_user'):
            href = link.find('a').get('href')
            friends_url_list.append(href)
        return friends_url_list

    def follow(self, url):
            if (self.login_status):
                while True:
                    action, status = self.add_to_friend(self.get(self.LOGIN_URL + url))
                    if action == 'captha' and status:
                        self.follow_counter += 1
                        VkAccountFriendsList.objects.create(
                            account_id=self.account_id,
                            account_href=url
                        )
                        self.write_log("Добавлен пользователь: " + self.VK_URL + url, 0)
                        return False
                    elif action == 'add' and status:
                        self.follow_counter += 1
                        VkAccountFriendsList.objects.create(
                            account_id=self.account_id,
                            account_href=url
                        )
                        self.write_log("Добавлен пользователь: " + self.VK_URL + url, 0)
                        return False
                    elif action == 'add' and not status:
                        return False

    def auto_delete_3_days(self, list_id, list_href, is_delete=False):
        i = 0
        for href in list_href:
            url = self.LOGIN_URL + href
            status, link = self.is_in_friends(url)
            # Если добавил в друзья то удаляй его
            if status:
                is_delete_friends = TaskAddToFriends.objects.filter(account_id=self.account_id).values('is_delete_friends').first()['is_delete_friends']
                if is_delete_friends:
                    if delete_of_friends:
                        self.get(self.LOGIN_URL + link)
                        delete_id = VkAccountFriendsList.objects.get(id=list_id[i])
                        delete_id.deleted = True
                        delete_id.save()
                        self.write_log("Удален из друзей пользователь: " + url, 0)
                    else:
                        time_to_delete = TaskAddToFriends.objects.filter(account_id=self.account_id).values('is_delete_friends_time').first()['is_delete_friends_time']
                        if time_to_delete == 0:
                            self.get(self.LOGIN_URL + link)
                            delete_id = VkAccountFriendsList.objects.get(id=list_id[i])
                            delete_id.deleted = True
                            delete_id.save()
                            self.write_log("Удален из друзей пользователь: " + url, 0)
                        else:
                            delete_id = VkAccountFriendsList.objects.get(id=list_id[i])
                            delete_id.deleted = False
                            delete_id.add_to_friend = True
                            delete_id.datetime_3_days = datetime.now() + timedelta(seconds=300)
                            delete_id.save()
                            self.write_log("Пользователь добавил вас в друзья, скоро он будет удален: " + url, 0)
                else:
                    self.write_log("Пользователь добавил вас в друзья : " + url, 0)
                    delete_id = VkAccountFriendsList.objects.get(id=list_id[i])
                    delete_id.add_to_friend = True
                    delete_id.datetime_3_days = datetime.now() + timedelta(days=10000)
                    delete_id.save()

            elif not status and link == None:
                self.write_log("Пользователь еще не добавил вас в друзья : " + url, 0)
                update_id = VkAccountFriendsList.objects.get(id=list_id[i])
                update_id.datetime_3_days = datetime.now() + timedelta(days=10000)
                update_id.save()
            elif not status and link == 'blocked':
                delete_id = VkAccountFriendsList.objects.get(id=list_id[i])
                delete_id.delete()
                self.write_log("Удален из списка друзей пользователь: " + url, 0)
                i+=1
                time.sleep(5)
                continue
            i+=1
            time.sleep(5)

    def auto_unfollow_10_days(self, list_id, list_href):
        i = 0
        for href in list_href:
            url = self.LOGIN_URL + href
            status, link = self.is_in_friends(url)
            # Если добавил в друзья то удаляй его
            if status:
                is_delete_friends = TaskAddToFriends.objects.filter(account_id=self.account_id).values('is_delete_friends').first()['is_delete_friends']
                if is_delete_friends:
                    time_to_delete = TaskAddToFriends.objects.filter(account_id=self.account_id).values('is_delete_friends_time').first()['is_delete_friends_time']
                    if time_to_delete == 0:
                        self.get(self.LOGIN_URL + link)
                        delete_id = VkAccountFriendsList.objects.get(id=list_id[i])
                        delete_id.deleted = True
                        delete_id.save()
                        self.write_log("Удален из друзей пользователь: " + url, 0)
                    else:
                        delete_id = VkAccountFriendsList.objects.get(id=list_id[i])
                        delete_id.deleted = False
                        delete_id.add_to_friend = True
                        delete_id.datetime_3_days = datetime.now() + timedelta(seconds=300)
                        delete_id.save()
                        self.write_log("Пользователь добавил вас в друзья, скоро он будет удален: " + url, 0)
                else:
                    self.write_log("Пользователь добавил вас в друзья : " + url, 0)
                    delete_id = VkAccountFriendsList.objects.get(id=list_id[i])
                    delete_id.datetime_3_days = datetime.now() + timedelta(days=10000)
                    delete_id.add_to_friend = True
                    delete_id.save()
            # Иначе отписывайся
            elif not status and link == None:
                status, link = self.get_unfollow_link(url)
                if status:
                    self.get(self.LOGIN_URL + link)
                    delete_id = VkAccountFriendsList.objects.get(id=list_id[i])
                    delete_id.unfollow = True
                    delete_id.save()
                    self.write_log("Отписался от пользователя: " + url, 0)
                else:
                    delete_id = VkAccountFriendsList.objects.get(id=list_id[i])
                    delete_id.delete()
                    self.write_log("Удален из списка друзей пользователь: " + url, 0)
                    i+=1
                    time.sleep(5)
                    continue
            elif not status and link == 'blocked':
                delete_id = VkAccountFriendsList.objects.get(id=list_id[i])
                delete_id.delete()
                self.write_log("Удален из списка друзей пользователь: " + url, 0)
                i+=1
                time.sleep(5)
                continue
            i+=1
            time.sleep(5)

    def datetime_limit_days_end(self):
        dt = datetime.now()
        end = dt.replace(year=dt.timetuple().tm_year, month=dt.timetuple().tm_mon, day=dt.timetuple().tm_mday, hour=23, minute=59, second=59)
        return dt + timedelta(seconds=(end - dt).total_seconds())

    def datetime_wait_recomended_friends(self):
        return datetime.now() + timedelta(days=1)

    def auto_follow(self):
        friends_url_list = self.friends_recomented_list()
        count_for_exit = len(friends_url_list)
        if count_for_exit == 0:
            self.write_log("Закончились рекомендованные друзья.", 0)
            vk_acc = VkAccount.objects.get(id=self.account_id)
            vk_acc.datetime_wait_recomended_friends = self.datetime_wait_recomended_friends()
            vk_acc.status = 4
            vk_acc.save()
            return
        # вк выдает 30 рекомендованных,
        # надо как то когда кончатся обновить список
        # if i == 29 то  обновить, пересчитать колво для выхода
        # и сравнить с числом добавлений в день
        i=0
        while True:
            if time.time() > self.next_iteration["Follow"] and \
                            self.follow_per_day != 0:
                self.follow(friends_url_list[i])
                self.next_iteration["Follow"] = time.time() + \
                                            self.add_time(self.follow_delay)
                i=i+1
                if i + 1 >= count_for_exit or i ==  self.follow_per_day:
                    return False
            time.sleep(5)
