from django.db.models.signals import post_save, pre_save
from socialprom.modules.proxy.models import Proxy, ProxyToAccount
from socialprom.modules.social.vkontakte.models import VkAccount
from django.dispatch import receiver

@receiver(post_save)
def vk_account_create(sender, instance, created, **kwargs):
    if issubclass(sender, VkAccount) and created:
        if not instance.proxy:
            proxy_list = Proxy.objects.filter(active=True)
            if proxy_list:
                for proxy in proxy_list:
                    if ProxyToAccount.objects.filter(proxy_id=proxy.id).count() < proxy.max_accounts:
                        if proxy.login and proxy.password:
                            proxy_sting = '%s:%s@%s:%s' % (
                                proxy.login, proxy.password, proxy.ip, proxy.port
                            )
                            ProxyToAccount.objects.create(proxy_id=proxy.id)
                            instance.proxy = proxy_sting
                            instance.save()
                        else:
                            proxy_sting = '%s:%s' % (
                                proxy.ip, proxy.port
                            )
                            ProxyToAccount.objects.create(proxy_id=proxy.id)
                            instance.proxy = proxy_sting
                            instance.save()
                            return
            #ToDo send notifications user, not free proxy
