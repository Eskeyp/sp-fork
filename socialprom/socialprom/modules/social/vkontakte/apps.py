from django.apps import AppConfig


class VkontakteConfig(AppConfig):
    name = 'socialprom.modules.social.vkontakte'

    def ready(self):
        from socialprom.modules.social.vkontakte import signals
