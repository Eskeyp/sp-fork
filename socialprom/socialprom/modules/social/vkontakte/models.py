from django.db import models
from socialprom.modules.social.models import SocialAccount
from django.utils.translation import ugettext_lazy as _
from datetime import datetime, timedelta
from socialprom.modules.social.limits.models import VkAccountLimits

from django.conf import settings

class VkAccount(SocialAccount):
    name = "Вконтакте"
    phone = models.CharField(_('phone'), max_length=100, blank=True)
    level = models.ForeignKey(VkAccountLimits, on_delete=models.PROTECT, null=True)
    datetime_limit_days_end = models.DateTimeField(
        "Ограничение до конца дня",
        default=datetime.now(),
    )
    datetime_wait_recomended_friends = models.DateTimeField(
        "Ожидание появления рекомендованных друзей",
        default=datetime.now(),
    )
    auth_code = models.CharField(_('auth_code'), max_length=12, blank=True)


    def __str__(self):
        return self.login

    def save(self, *args, **kwargs):
        if self.level is None:

            self.level = VkAccountLimits.objects.filter(status=VkAccountLimits.Status.LOW).first()
        super(VkAccount, self).save(*args, **kwargs)

class TaskAddToFriends(models.Model):
    """
    Задача на добавления в друзья из списка рекомендованных
    """
    name = models.CharField(_('Название задачи'), max_length=100, blank=True)
    account = models.ForeignKey(VkAccount, on_delete=models.CASCADE)
    active = models.BooleanField(_('active'), default=False)
    is_delete_friends = models.BooleanField(_('active'), default=True)
    is_delete_friends_time = models.PositiveIntegerField(
        "Количество дней до удаления из друзей",
         default=0
         )

    def __str__(self):
        return self.name


def datetime_3_days_end_date():
    return settings.VK_DELAY_TO_DELETE_3_DAYS #days 3

def datetime_10_days_end_date():
    return settings.VK_DELAY_TO_UNFOLLOW_10_DAYS #days 10

class VkAccountFriendsList(models.Model):
    """
    Список пользователей на удаление из друзей
    """
    account = models.ForeignKey(VkAccount, on_delete=models.CASCADE)
    account_href =  models.CharField(_('Ссылка на пользователя'), max_length=100)
    datetime_3_days = models.DateTimeField(
        "Время добавления",
        default=settings.VK_DELAY_TO_DELETE_3_DAYS,
    )
    datetime_10_days = models.DateTimeField(
        "Время добавления",
        default=settings.VK_DELAY_TO_UNFOLLOW_10_DAYS,
    )
    deleted = models.BooleanField(_('deleted'), default=False)
    add_to_friend = models.BooleanField(_('add_to_friend'), default=False)
    unfollow = models.BooleanField(_('unfollow'), default=False)
    forced_delete = models.BooleanField(_('forced_delete'), default=False)


class VkAccountLogsList(models.Model):

    class Level:
        INFO = "0"
        EROOR = "1"
        WARNING = "2"

        choices = (
            (INFO, 'INFO'),
            (EROOR, 'ERROR'),
            (WARNING, 'WARNING'),
        )

    account = models.ForeignKey(VkAccount, on_delete=models.CASCADE)
    message =  models.CharField(_('Сообщение'), max_length=150)
    status = models.CharField(_('Статус'),
        max_length=1,
        choices=Level.choices,
        default=Level.INFO
    )
    datetime = models.DateTimeField(
        "Время записи лога",
        default=datetime.now,
    )
