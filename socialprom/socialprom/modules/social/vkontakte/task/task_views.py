from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from socialprom.modules.social.vkontakte.task.task_serializers import CreateTaskAddToFriendsSerializer,\
ListTaskSerializer
from rest_framework.permissions import IsAuthenticated
from django.shortcuts import get_object_or_404
from rest_framework import generics
from socialprom.modules.social.vkontakte.models import TaskAddToFriends
from socialprom.modules.social.vkontakte.models import VkAccount
from socialprom.modules.social.vkontakte.tasks import add_to_friends
from socialprom.accounts.models import User, CsrfExemptSessionAuthentication

class CreateTaskAddToFriends(APIView):

    authentication_classes = (CsrfExemptSessionAuthentication, )
    permission_classes = (IsAuthenticated, )

    def post(self, request, format='json'):
        data = request.data
        try:
            account_id = data['account_id']
            task_name = data['task_name']
        except:
            account_id = None
            task_name = None
            return Response({
                    "detail": 'Ошибка в запросе, укажите аккаунт и название задачи'},
                    status=status.HTTP_400_BAD_REQUEST)
        try:
            is_delete_friends = data['is_delete_friends']
        except:
            is_delete_friends = None

        try:
            is_delete_friends_time = data['is_delete_friends_time']
        except:
            is_delete_friends_time = None

        if request.user and account_id and task_name:
            stat = VkAccount.objects.get(id=account_id).status
            if stat == VkAccount.Status.NOAUTH:
                return Response({
                        "detail": 'Аккаунт требует авторизации',
                        "status": 'Ожидает авторизации'},
                        status=status.HTTP_401_UNAUTHORIZED)
            elif stat == VkAccount.Status.OFF:
                return Response({
                        "detail": 'Аккаунт требует включения и проверки авторизации',
                        "status": 'Ожидает включения'},
                        status=status.HTTP_400_BAD_REQUEST)
            task = TaskAddToFriends.objects.create(account_id=account_id, name=task_name)
            if task:
                task.active = True

                if is_delete_friends:
                    task.is_delete_friends = is_delete_friends

                if is_delete_friends_time:
                    task.is_delete_friends_time = is_delete_friends_time

                task.save()
                return Response({
                        "success": "Задача активирована",
                        "status":task.active },
                         status=status.HTTP_200_OK
                         )
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        else:
            return Response({
                "detail": 'Ошибка в запросе, укажите аккаунт и название задачи'},
                status=status.HTTP_401_UNAUTHORIZED)

class DeleteVkTasks(APIView):

    permission_classes = (IsAuthenticated,)
    authentication_classes = (CsrfExemptSessionAuthentication, )

    def delete(self, request, pk, format='json'):
        if pk:
            instance = get_object_or_404(TaskAddToFriends, account__user_id=request.user.id, id=pk)
            instance.delete()
            return Response({
                    "success": "Задача удалена"},
                    status=status.HTTP_202_ACCEPTED
                    )
        else:
            return Response({"detail": 'Задача не найден'}, status=status.HTTP_404_NOT_FOUND)

class ListTasks(generics.ListAPIView):

    serializer_class = ListTaskSerializer
    permission_classes = (IsAuthenticated, )

    def get_queryset(self):
        if self.request.user:
            queryset = TaskAddToFriends.objects.filter(account__user_id=self.request.user.id).order_by('id')
            return queryset
        else:
            return Response(serializer.errors, status=status.HTTP_401_UNAUTHORIZED)

class DeactivateTask(APIView):
    authentication_classes = (CsrfExemptSessionAuthentication, )
    permission_classes = (IsAuthenticated,)

    def post(self, request, format='json'):
        data = request.data
        try:
            task_id = data['task_id']
        except:
            return Response({"detail": 'Ошибка запроса'},status=status.HTTP_400_BAD_REQUEST)

        if task_id:
            instance = get_object_or_404(TaskAddToFriends, account__user_id=request.user.id, id=task_id)
            instance.active = False
            instance.save()
            return Response({
                    "success": "Задача остановлена",
                    "status":instance.active },
                     status=status.HTTP_200_OK
                     )
        else:
            return Response({"detail": 'Ошибка запроса'},status=status.HTTP_400_BAD_REQUEST)

class ActivateTask(APIView):

    authentication_classes = (CsrfExemptSessionAuthentication, )
    permission_classes = (IsAuthenticated,)

    def post(self, request, format='json'):
        data = request.data
        task_id = data['task_id']

        if task_id:
            instance = get_object_or_404(TaskAddToFriends, account__user_id=request.user.id, id=task_id)
            if instance.account.proxy:
                proxy = instance.account.proxy
            else:
                proxy = ""
                #todo get free proxy in proxy_list
            if instance.account.status == VkAccount.Status.ACTIVE:
                instance.active = True
                instance.save()
                #todo task.delay for celery no retutn
                #add_to_friends(instance.account.id)
                return Response({
                        "success": "Задача активирована",
                        "status":instance.active },
                         status=status.HTTP_200_OK
                         )
            elif instance.account.status == VkAccount.Status.NOAUTH or instance.account.status == VkAccount.Status.ERROR:
                return Response({
                        "detail": 'Аккаунт требует авторизации',
                        "status":instance.account.get_status_display()},
                        status=status.HTTP_401_UNAUTHORIZED)
            elif instance.account.status == VkAccount.Status.OFF:
                return Response({
                        "detail": 'Аккаунт требует включения и проверки авторизации',
                        "status":instance.account.get_status_display()},
                        status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({"detail": 'Ошибка запроса'},status=status.HTTP_400_BAD_REQUEST)
