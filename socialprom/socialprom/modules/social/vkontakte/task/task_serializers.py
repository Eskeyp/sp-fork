from rest_framework import serializers
from socialprom.modules.social.vkontakte.models import TaskAddToFriends


class CreateTaskAddToFriendsSerializer(serializers.ModelSerializer):

    class Meta:
        model = TaskAddToFriends
        fields = ('id', 'name', 'account','is_delete_friends','is_delete_friends_time')

class ListTaskSerializer(serializers.ModelSerializer):
    account = serializers.SerializerMethodField()
    account_image = serializers.SerializerMethodField()

    class Meta:
        model = TaskAddToFriends
        fields = ('id', 'name', 'account', 'account_id', 'account_image', 'active', 'is_delete_friends', 'is_delete_friends_time')

    def get_account(self, data):
        return data.account.login

    def get_account_image(self, data):
        try:
            return data.account.avatar.url
        except:
            return None
