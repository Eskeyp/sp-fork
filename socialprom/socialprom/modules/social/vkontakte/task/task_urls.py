from django.conf.urls import url
from socialprom.modules.social.vkontakte.task import task_views

urlpatterns = [
    url(r'^create/$', task_views.CreateTaskAddToFriends.as_view(), name='task-create'),
    url(r'^list/$', task_views.ListTasks.as_view(), name='task-list'),
    url(r'^activate/$', task_views.ActivateTask.as_view(), name='task-activate'),
    url(r'^deactivate/$', task_views.DeactivateTask.as_view(), name='task-deactivate'),
    url(r'^delete/(?P<pk>[0-9]+)/$', task_views.DeleteVkTasks.as_view(), name='task-delete'),

]
