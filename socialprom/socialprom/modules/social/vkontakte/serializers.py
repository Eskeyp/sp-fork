from rest_framework import serializers
import re
from django.conf import settings
from datetime import datetime, timedelta
import string
import random
from socialprom.modules.social.vkontakte.models import VkAccount,  VkAccountLogsList, VkAccountFriendsList
from socialprom.modules.social.limits.models import VkAccountLimits

def proxy_validators(proxy):
    if proxy:
        m = re.match("^(?:(\w+)(?::(\w+))?@)?((?:\d{1,3})(?:\.\d{1,3}){3})(?::(\d{1,5}))?$", proxy)
        if m is None:
            raise serializers.ValidationError("Не правильно указан proxy, укажите валидный IPV4 прокси")
        return proxy
    return None

class CreateVkAccountSerializer(serializers.ModelSerializer):

    login = serializers.CharField(
            max_length=100,
            write_only=True,
            required=True
            )

    password = serializers.CharField(
            write_only=True,
            required=True,
            )

    phone = serializers.CharField(
            required=False,
            )

    proxy = serializers.CharField(
            max_length=100,
            required=False,
            validators=[proxy_validators]
            )

    class Meta:
        model = VkAccount
        fields = ('id', 'login', 'password', 'phone', 'proxy',)
        read_only_fields = ('name',)

class UpdateVkAccountSerializer(serializers.ModelSerializer):

    login = serializers.CharField(
            max_length=100,
            required=False,
            )

    password = serializers.CharField(
            required=False,
            )

    phone = serializers.CharField(

            )

    proxy = serializers.CharField(
            max_length=100,
            required=False,
            validators=[proxy_validators]
            )

    class Meta:
        model = VkAccount
        fields = ('id', 'login', 'password', 'phone', 'proxy',)
        read_only_fields = ('name',)

class ListVkAccountSerializer(serializers.ModelSerializer):
    status = serializers.SerializerMethodField()
    level = serializers.SerializerMethodField()
    proxy = serializers.SerializerMethodField()
    status_id = serializers.SerializerMethodField()
    avatar = serializers.SerializerMethodField()

    class Meta:
        model = VkAccount
        fields = ("id", "login", 'phone', "status", "status_id", 'level',"proxy", 'avatar')

    def get_avatar(self, data):
        try:
            return data.avatar.url
        except:
            return None

    def get_proxy(self, data):
        proxy = data.proxy
        if proxy:
            return proxy[proxy.find('@')+1:len(proxy)]
        return None

    def get_status(self, data):
        return data.get_status_display()

    def get_status_id(self, data):
        return data.status

    def get_level(self, data):
        return VkAccountLimits.objects.get(id=data.level.id).get_status_display()

class ListFriendsVkAccountSerializer(serializers.ModelSerializer):
    account = serializers.SerializerMethodField()
    account_href = serializers.SerializerMethodField()

    class Meta:
        model = VkAccountFriendsList
        fields = ("id", "account", "account_href", 'datetime_3_days', "datetime_10_days", 'deleted', 'unfollow')

    def get_account(self, data):
        return data.account.login

    def get_account_href(self, data):
        return 'https://vk.com' + data.account_href

class VkAccountLogsListSerializer(serializers.ModelSerializer):
    status = serializers.SerializerMethodField()

    class Meta:
        model = VkAccountLogsList
        fields = ("id", "status", "message", 'datetime')

    def get_status(self, data):
        return data.get_status_display()


class DetailVkAccountSerializer(serializers.ModelSerializer):
    password = serializers.SerializerMethodField()
    status = serializers.SerializerMethodField()

    class Meta:
        model = VkAccount
        fields = ("id", "login", 'password', 'phone', "proxy", 'status',)

    def get_status(self, data):
        status = {}
        status['id'] = data.status
        status['name'] = data.get_status_display()
        return status

    def get_password(self, data):
        return len(data.password)

class ActivateVkAccountSerializer(serializers.ModelSerializer):

    class Meta:
        model = VkAccount
        fields = ("id", "status")
