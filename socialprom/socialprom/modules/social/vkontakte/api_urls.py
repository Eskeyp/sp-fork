from django.conf.urls import url
from socialprom.modules.social.vkontakte import views

app_name="vkontakte"

urlpatterns = [
    url(r'^create/$', views.CreateVkAccount.as_view(), name='account-create'),
    url(r'^detail/(?P<account_id>[0-9]+)/$', views.DetailVkAccount.as_view(), name='account-detail'),
    url(r'^list/$', views.ListVkAccount.as_view(), name='account-list'),
    url(r'^activate/$', views.ActivateVkAccount.as_view(), name='account-activate'),
    url(r'^auth_code/$', views.AuthCheckCodeVkAccount.as_view(), name='account-auth_code'),
    url(r'^deactivate/$', views.DeactivateVkAccount.as_view(), name='account-deactivate'),
    url(r'^delete/(?P<pk>[0-9]+)/$', views.DeleteVkAccount.as_view(), name='account-delete'),
    url(r'^update/(?P<account_id>[0-9]+)/$', views.UpdateVkAccount.as_view(), name='account-detail'),
    url(r'^logs/(?P<account_id>[0-9]+)/$', views.LogVkAccount.as_view(), name='account-logs'),
    url(r'^friends_list/(?P<account_id>[0-9]+)/$', views.ListVkAccountFriendsList.as_view(), name='account-friends_list'),

]
