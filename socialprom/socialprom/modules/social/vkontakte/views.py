from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from socialprom.modules.social.vkontakte.serializers import CreateVkAccountSerializer, ListVkAccountSerializer, \
ActivateVkAccountSerializer, DetailVkAccountSerializer, UpdateVkAccountSerializer, VkAccountLogsListSerializer, \
ListFriendsVkAccountSerializer
from socialprom.modules.social.vkontakte.models import VkAccount, VkAccountLogsList, VkAccountFriendsList
from rest_framework.permissions import IsAuthenticated, IsAuthenticated
from django.utils.translation import ugettext_lazy as _
from rest_framework.permissions import AllowAny
from rest_framework import generics
from django.shortcuts import get_object_or_404
from socialprom.modules.social.vkontakte.lib import Vk
from socialprom.modules.social.limits.models import VkAccountLimits
from django.shortcuts import render_to_response
from django.contrib.auth import authenticate
from socialprom.accounts.models import User, CsrfExemptSessionAuthentication
from django.http import Http404
from django.core import serializers


def accounts_page(request):
    if request.user.is_authenticated:
        return render_to_response("vk.html")
    raise Http404

class CreateVkAccount(APIView):
    authentication_classes = (CsrfExemptSessionAuthentication, )
    serializer_class = CreateVkAccountSerializer
    permission_classes = (IsAuthenticated, )

    def post(self, request, format='json'):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            if request.user:
                vk = serializer.save(user_id=request.user.id)
                if vk:
                    return Response({"id": vk.id, "status": vk.get_status_display(), 'status_id':vk.status}, status=status.HTTP_201_CREATED)
            else:
                return Response(serializer.errors, status=status.HTTP_401_UNAUTHORIZED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class UpdateVkAccount(APIView):

    authentication_classes = (CsrfExemptSessionAuthentication, )
    serializer_class = UpdateVkAccountSerializer
    permission_classes = (IsAuthenticated, )

    def check_account(self, account_id, login, password, phone=None, proxy=""):
        obj = Vk(login, password, proxy=proxy, account_id=account_id)
        status_code, message = obj.login()
        if status_code:
            return True, message
        else:
            return False, message

    def get_object(self, account_id, user_id):
        objects = VkAccount.objects.filter(user_id=user_id, id=account_id).first()
        if objects:
            return objects
        raise Http404

    def put(self, request, account_id, format='json'):
        snippet = self.get_object(account_id, request.user.id)
        serializer = self.serializer_class(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            snippet.status = VkAccount.Status.NOAUTH
            snippet.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class DetailVkAccount(generics.RetrieveAPIView):
    serializer_class = DetailVkAccountSerializer
    permission_classes = (IsAuthenticated, )
    queryset = ("id", "login", 'password', 'phone', "proxy", 'status')
    lookup_url_kwarg = 'account_id'

    def get_queryset(self):
        queryset = VkAccount.objects.filter(user_id=self.request.user.id, id=self.kwargs['account_id'])
        if queryset:
            return queryset
        return None

class ListVkAccount(generics.ListAPIView):
    serializer_class = ListVkAccountSerializer
    permission_classes = (IsAuthenticated, )

    def get_queryset(self):
        if self.request.user:
            queryset = VkAccount.objects.filter(user_id=self.request.user.id)
            return queryset
        else:
            return Response(serializer.errors, status=status.HTTP_401_UNAUTHORIZED)

class ListVkAccountFriendsList(generics.ListAPIView):
    serializer_class = ListFriendsVkAccountSerializer
    permission_classes = (IsAuthenticated, )

    lookup_url_kwarg = 'account_id'

    def get_queryset(self):
        queryset = VkAccountFriendsList.objects.filter(
            account__user_id=self.request.user.id,
            account_id=self.kwargs['account_id']
        )
        if queryset:
            return queryset
        return None

class LogVkAccount(generics.ListAPIView):
    serializer_class = VkAccountLogsListSerializer
    permission_classes = (IsAuthenticated, )
    lookup_url_kwarg = 'account_id'

    def get_queryset(self):
        if self.request.user:
            queryset = VkAccountLogsList.objects.filter(
                account_id=self.kwargs['account_id'],
                account__user_id=self.request.user.id
            ).order_by('-id')
            return queryset
        else:
            return Response(serializer.errors, status=status.HTTP_401_UNAUTHORIZED)

class AuthCheckCodeVkAccount(APIView):
    authentication_classes = (CsrfExemptSessionAuthentication, )
    permission_classes = (IsAuthenticated,)

    def post(self, request, format='json'):
        data = request.data
        account_id = data['account_id']
        code = data['code']
        if account_id:
            instance = get_object_or_404(VkAccount, user_id=request.user.id, id=account_id)
            instance.auth_code = code
            instance.save()
            return Response({"success": 'true'},
                    status=status.HTTP_200_OK
                    )
        else:
            return Response({"detail": 'Ошибка в запросе'},status=status.HTTP_400_BAD_REQUEST)

class ActivateVkAccount(APIView):
    authentication_classes = (CsrfExemptSessionAuthentication, )
    permission_classes = (IsAuthenticated,)

    def check_account(self, login, password, phone=None, proxy="", account_id=None):
        obj = Vk(login, password, proxy=proxy, account_id=account_id)
        status_code, message = obj.login()
        if status_code:
            image = obj.save_user_avatar()
            obj.logout()
            return True, message, image
        else:
            return False, message, None

    def post(self, request, format='json'):
        data = request.data
        account_id = data['account_id']

        if account_id:
            instance = get_object_or_404(VkAccount, user_id=request.user.id, id=account_id)
            if instance.proxy:
                proxy = instance.proxy
            else:
                proxy = ""
                #todo get free proxy in proxy_list
            status_code, message, image = self.check_account(instance.login, instance.password, proxy=proxy, account_id=instance.id)

            if status_code:
                instance.status = VkAccount.Status.ACTIVE
                instance.avatar = image
                instance.save()
                return Response({
                        "success": message,
                        "status":instance.get_status_display() },
                         status=status.HTTP_200_OK
                         )
            else:
                instance.status = VkAccount.Status.ERROR
                instance.save()
                return Response({
                        "detail": message,
                        "status":instance.get_status_display()},
                        status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({"detail": 'Ошибка в запросе'},status=status.HTTP_400_BAD_REQUEST)

class DeactivateVkAccount(APIView):
    authentication_classes = (CsrfExemptSessionAuthentication, )
    permission_classes = (IsAuthenticated,)

    def post(self, request, format='json'):
        data = request.data
        account_id = data['account_id']
        if account_id:
            instance = get_object_or_404(VkAccount, user_id=request.user.id, id=account_id)
            instance.status = VkAccount.Status.OFF
            instance.save()
            return Response({
                    "success": "Аккаунт отключен",
                    "status":instance.get_status_display()},
                    status=status.HTTP_200_OK
                    )
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)

class DeleteVkAccount(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (CsrfExemptSessionAuthentication, )

    def delete(self, request, pk, format='json'):
        if pk:
            instance = get_object_or_404(VkAccount, user_id=request.user.id, id=pk)
            instance.delete()
            return Response({
                    "success": "Аккаунт удален"},
                    status=status.HTTP_202_ACCEPTED
                    )
        else:
            return Response({"detail": 'Аккаунт не найден'}, status=status.HTTP_404_NOT_FOUND)
