from django.contrib import admin

from socialprom.modules.social.limits.models import VkAccountLimits

class VkAccountLimitsAdmin(admin.ModelAdmin):
    list_display =('status','add_friends','likes','follow_group', 'repost')

admin.site.register(VkAccountLimits, VkAccountLimitsAdmin)
