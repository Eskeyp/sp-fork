from django.apps import AppConfig


class LimitsConfig(AppConfig):
    name = 'limits'
    verbose_name = 'Ограничения соц сетей'
