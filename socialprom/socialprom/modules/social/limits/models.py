from django.db import models

class VkAccountLimits(models.Model):

    class Status:
        LOW = "0"
        MIDLLE = "1"
        HIGH = "2"

        choices = (
            (LOW, 'Новый аккаунт'),
            (MIDLLE, 'Средний аккаунт'),
            (HIGH, 'Старый аккаунт'),
        )

    add_friends = models.PositiveIntegerField(
        "Cуточный лимит на приглашения в друзья",
         blank=True, null=True
         )
    likes = models.PositiveIntegerField(
        "Cуточный лимит на лайки аккаунтов",
         blank=True, null=True
         )
    follow_group = models.PositiveIntegerField(
        "Cуточный лимит на приглашения в группу/на мероприятие",
         blank=True, null=True
         )
    repost = models.PositiveIntegerField(
        "Cуточный лимит на репосты",
         blank=True, null=True
         )
    status = models.CharField(
        'Тип аккаунта',
        max_length=1,
        choices=Status.choices,
        default=Status.LOW
    )

    def __str__(self):
        return self.get_status_display()

    def get_status(self):
        return self.get_status_display()
