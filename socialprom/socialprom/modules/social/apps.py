from django.apps import AppConfig


class SocialConfig(AppConfig):
    name = 'socialprom.modules.social'
