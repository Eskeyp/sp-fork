from django.contrib import admin

from socialprom.modules.proxy.models import Proxy, ProxyToAccount

class ProxyAdmin(admin.ModelAdmin):
    list_display =('__str__', 'active', 'max_accounts', 'get_count')

    def get_count(self, obj):
        return ProxyToAccount.objects.filter(proxy_id=obj.id).count()
    
    get_count.short_description = 'count'
admin.site.register(Proxy, ProxyAdmin)
