from django.apps import AppConfig


class ProxyConfig(AppConfig):
    name = 'socialprom.modules.proxy'
    verbose_name = 'Ограничения соц сетей'
