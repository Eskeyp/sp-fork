from django.db import models
from django.utils.translation import ugettext_lazy as _

class Proxy(models.Model):

    ip = models.CharField(_('ip'), max_length=20)
    port = models.PositiveIntegerField(_('port'), default=8080)
    login = models.CharField(_('login'), max_length=50, blank=True)
    password = models.CharField(_('password'), max_length=50, blank=True)
    active = models.BooleanField(_('active'), default=False)
    max_accounts = models.PositiveIntegerField(_('max_accounts'), default=2)

    def __str__(self):
        return self.ip + ":" + str(self.port)

class ProxyToAccount(models.Model):

    proxy = models.ForeignKey(Proxy, on_delete=models.PROTECT)
