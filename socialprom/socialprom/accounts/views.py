from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from socialprom.accounts.serializers import UserCreateSerializer, LoginSerializer, PasswordResetSerializer, PasswordChangeSerializer
from socialprom.accounts.models import User, CsrfExemptSessionAuthentication
#from rest_framework.authtoken.models import Token
from django.conf import settings
from rest_framework.permissions import AllowAny
from django.core.mail import send_mail
from django.urls import reverse
from django.shortcuts import redirect, get_object_or_404
from datetime import datetime, timedelta
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import authenticate
from django.contrib.auth import login as _login
from django.views.decorators.csrf import csrf_exempt
import hashlib
from datetime import datetime, timedelta
import random

RECOVER_EMAIL_MESSAGE = """
Для того чтобы активировать новый аккаунт, воспользуйтесь ссылкой:
http://{domain_name}{path}\n\n
"""
RECOVER_PASSWORD_MESSAGE = """
Для того чтобы изменить пароль, воспользуйтесь ссылкой:
http://{domain_name}{path}\n\n
"""

class UserCreate(APIView):

    authentication_classes = (CsrfExemptSessionAuthentication, )
    serializer_class = UserCreateSerializer
    permission_classes = (AllowAny,)

    """
    Create user
    """
    def post(self, request, format='json'):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            user = serializer.save()
            if user:
                json = serializer.data
                return Response(json, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class SignupVerify(APIView):
    permission_classes = (AllowAny,)
    authentication_classes = (CsrfExemptSessionAuthentication, )

    def get(self, request, user_email, activation_code, format=None):
        user = get_object_or_404(User, email=user_email, activation_key=activation_code)
        if user.key_expires < timezone.now():
            json = {'detail': _('Unable to verify user.')}
            return Response(json, status=status.HTTP_301_MOVED_PERMANENTLY)

        user.key_expires = datetime.now(tz=None)
        user.is_active = True
        user.save()
        json = {'success': _('User verified.')}
        return Response(json, status=status.HTTP_200_OK)

class UserSetPassword(APIView):
    permission_classes = (AllowAny,)
    def get(self, request, user_email, set_password, format=None):
        user = get_object_or_404(User, email=user_email, activation_key=set_password)
        if user.key_expires < timezone.now():
            json = {'detail': _('Время смены пароля истекло, ссылка больше не активна, повторите попытку')}
            return Response(json, status=status.HTTP_301_MOVED_PERMANENTLY)
        json = {'success': _('Для продолжения введите новый пароль'), 'email':user.email}
        return Response(json, status=status.HTTP_200_OK)

    def post(self, request, format='json', **kwargs):
        data = request.data
        user = get_object_or_404(User, email=data['email'])

        if user and data['password'] and data['confirm_password']:

            if user.key_expires < timezone.now():
                json = {'detail': _('Время смены пароля истекло, ссылка больше не активна, повторите попытку')}
                return Response(json, status=status.HTTP_301_MOVED_PERMANENTLY)

            if data['password'] != data['confirm_password']:
                return Response({'detail': _('Пароли должны совпадать')}, status=status.HTTP_400_BAD_REQUEST)

            user.set_password(data['password'])
            user.key_expires = datetime.now(tz=None)
            user.save()
            json = {'success': _('Пароль успешно изменен')}
            return Response(json, status=status.HTTP_200_OK)
        else:
            return Response({'detail': _('Укажите пароль и его подтверждение')}, status=status.HTTP_400_BAD_REQUEST)

class UserPasswordReset(APIView):
    authentication_classes = (CsrfExemptSessionAuthentication, )
    permission_classes = (AllowAny,)

    def post(self, request, format='json'):
        data = request.data
        user = get_object_or_404(User, email=data['email'])
        if user:
            salt = hashlib.sha1(str(random.randint(1, 1000)).encode()).hexdigest()[:5]
            activation_key = hashlib.sha1((salt).encode()).hexdigest()
            key_expires = datetime.now(tz=None) + timedelta(days=2)
            user.activation_key = activation_key
            user.key_expires = key_expires
            domain_name = request.META['HTTP_HOST']
            url_path = reverse(
                "accounts:reset-password",
                kwargs={'user_email': user.email, "set_password": user.activation_key}
                )
            email_body = RECOVER_PASSWORD_MESSAGE.format(domain_name=domain_name, path=url_path)
            email_subject = 'Восстановление пароля'
            send_mail(
                email_subject,
                email_body,
                'robot@smmrevolutions.ru',
                [user.email],
                fail_silently=False
            )
            user.save()
            json = {'success': _('На ваш электронный адрес отправлена инструкция, для смены пароля')}
            return Response(json, status=status.HTTP_200_OK)
        return Response({"detail": 'Пользователь не найден'}, status=status.HTTP_404_NOT_FOUND)

class UserLogin(APIView):

    authentication_classes = (CsrfExemptSessionAuthentication, )
    serializer_class = LoginSerializer
    permission_classes = (AllowAny,)

    def post(self, request, format='json'):
            serializer = self.serializer_class(data=request.data)
            if serializer.is_valid():
                email = serializer.data['email']
                password = serializer.data['password']
                user = authenticate(username=email, password=password)
                if user:
                    _login(request, user)
                    json = {'status': 'ok'}
                    return Response(json,
                        status=status.HTTP_200_OK)
                else:
                    json = {'detail': _('User account not active.')}
                    return Response(json,
                        status=status.HTTP_401_UNAUTHORIZED)
            else:
                return Response(serializer.errors,
                    status=status.HTTP_400_BAD_REQUEST)
