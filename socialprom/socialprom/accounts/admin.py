from django.contrib import admin

# Register your models here.
from socialprom.accounts.models import User

class UserAdmin(admin.ModelAdmin):
    list_display =('email','is_active', 'verify', 'date_joined')

admin.site.register(User, UserAdmin)
