from django.conf.urls import url
from socialprom.accounts import views

app_name="accounts"

urlpatterns = [
    url(r'^create/$', views.UserCreate.as_view(), name='account-create'),
    url(r'^activate/(?P<user_email>[^@]+@[^@]+)/(?P<activation_code>[0-9a-f]+)$', views.SignupVerify.as_view(), name='activate'),
    url(r'^login/$', views.UserLogin.as_view(), name='account-login'),
    url(r'^reset-password/$', views.UserPasswordReset.as_view(), name='account-resetpassword'),
    url(r'^set_password/(?P<user_email>[^@]+@[^@]+)/(?P<set_password>[0-9a-f]+)$', views.UserSetPassword.as_view(), name='reset-password'),
    url(r'^set_password/$', views.UserSetPassword.as_view(), name='account-setpassword'),
]
