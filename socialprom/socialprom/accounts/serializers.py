from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from django.conf import settings
from socialprom.accounts.models import User
import hashlib
from datetime import datetime, timedelta
import string
import random

class UserCreateSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(
            required=True,
            validators=[UniqueValidator(queryset=User.objects.all())]
            )
    password = serializers.CharField(
            min_length=6, max_length=100,
            write_only=True, required=True
            )
    confirm_password = serializers.CharField(
            write_only=True, required=True
            )

    def create(self, validated_data):
        user = User(email=validated_data['email'])

        if validated_data['password']:
            if validated_data['password'] != validated_data['confirm_password']:
                raise serializers.ValidationError(
                    "Пароли должны совпадать"
                )
        user.set_password(validated_data['password'])
        user.username = validated_data['email']

        salt = hashlib.sha1(str(random.randint(1, 1000)).encode()).hexdigest()[:5]
        activation_key = hashlib.sha1((salt).encode()).hexdigest()
        key_expires = datetime.now(tz=None) + timedelta(2)
        user.activation_key = activation_key
        user.key_expires = key_expires
        user.save()
        return user

    class Meta:
        model = User
        fields = ('id', 'email', 'password', 'confirm_password')
        read_only_fields = ('date_joined', 'date_updated')

class LoginSerializer(serializers.Serializer):
    email = serializers.EmailField(
            required=True,
            max_length=255,
            )
    password = serializers.CharField(
            min_length=6, max_length=100,
            required=True
            )
    class Meta:
        model = User
        fields = ('id', 'email', 'password')

class PasswordResetSerializer(serializers.Serializer):
    email = serializers.EmailField(
            required=True,
            )

class PasswordResetVerifiedSerializer(serializers.Serializer):
    code = serializers.CharField(max_length=40)
    password = serializers.CharField(
            min_length=6, max_length=100,
            required=True
            )

class PasswordChangeSerializer(serializers.Serializer):
    password = serializers.CharField(
            min_length=6, max_length=100,
            write_only=True, required=True
            )
    confirm_password = serializers.CharField(
            min_length=6, max_length=100,
            write_only=True, required=True
            )

    class Meta:
        model = User
        fields = ('password', 'confirm_password')
