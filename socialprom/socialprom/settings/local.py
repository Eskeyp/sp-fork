from .base import *
from datetime import datetime, timedelta

HOST = 'localhost:8000'

STATIC_URL = '/static/'
MEDIA_URL = '/media/'

DEBUG = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'socialprom',
        'USER': 'main',
        'PASSWORD': '12345',
        'HOST': 'localhost',
        'PORT': '',
    }
}
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
EMAIL_HOST = 'localhost'
EMAIL_PORT = 1025
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, '../../frontend/static'),
)

CELERY_BEAT_SCHEDULE = {
    'get_active_user_tasks': {
            'task': 'socialprom.modules.social.vkontakte.tasks.get_active_user_tasks',
            'schedule': crontab(minute='*/15'),
    },
    'get_friends_list_3_days': {
            'task': 'socialprom.modules.social.vkontakte.tasks.get_friends_list_3_days',
            'schedule': crontab(minute='*/7'),
    },
    'get_friends_list_10_days': {
            'task': 'socialprom.modules.social.vkontakte.tasks.get_friends_list_10_days',
            'schedule': crontab(minute='*/6'),
    },
    'remove_status': {
            'task': 'socialprom.modules.social.vkontakte.tasks.remove_status',
            'schedule': crontab(minute=0, hour='*/3'),
    },
}

VK_DELAY_TO_DELETE_3_DAYS = datetime.now() + timedelta(seconds=300)
VK_DELAY_TO_UNFOLLOW_10_DAYS = datetime.now() + timedelta(seconds=600)
