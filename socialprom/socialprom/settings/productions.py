from .base import *
from datetime import datetime, timedelta

ALLOWED_HOSTS = ['smmrevolutions.ru', '127.0.0.1']

STATIC_URL = '/static/'
MEDIA_URL = '/media/'

DEBUG = True
LANGUAGE_CODE = 'ru-RU'
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'socialprom',
        'USER': 'main',
        'PASSWORD': '12345',
        'HOST': 'localhost',
        'PORT': '',
    }
}
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'smtp.yandex.ru'
EMAIL_HOST_USER = 'robot@smmrevolution.ru'
EMAIL_HOST_PASSWORD = 'smmrevolution2018'
EMAIL_PORT = 587
EMAIL_USE_TLS = True
EMAIL_USE_SSL = False

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, '../../frontend/static'),
)

CELERY_BEAT_SCHEDULE = {
    'get_active_user_tasks': {
            'task': 'socialprom.modules.social.vkontakte.tasks.get_active_user_tasks',
            'schedule': crontab(minute=0, hour='*/1'),
    },
    'get_friends_list_3_days': {
            'task': 'socialprom.modules.social.vkontakte.tasks.get_friends_list_3_days',
            'schedule': crontab(minute=0, hour='*/1'),
    },
    'get_friends_list_10_days': {
            'task': 'socialprom.modules.social.vkontakte.tasks.get_friends_list_10_days',
            'schedule': crontab(minute=0, hour='*/2'),
    },
    'remove_status': {
            'task': 'socialprom.modules.social.vkontakte.tasks.remove_status',
            'schedule': crontab(minute=0, hour='*/1'),
    },
}


VK_DELAY_TO_DELETE_3_DAYS = datetime.now() + timedelta(seconds=259200)
VK_DELAY_TO_UNFOLLOW_10_DAYS = datetime.now() + timedelta(seconds=864000)
