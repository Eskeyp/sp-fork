import { createReducer } from '../'

export const GET_VK_ACCOUNTS = 'GET_VK_ACCOUNTS';
export const GET_VK_ACCOUNTS__SUCCESS = 'GET_VK_ACCOUNTS__SUCCESS';
export const GET_VK_ACCOUNTS__FAIL = 'GET_VK_ACCOUNTS__FAIL';
export const CREATE_VK_ACCOUNT = 'CREATE_VK_ACCOUNT';
export const CREATE_VK_ACCOUNT__SUCCESS = 'CREATE_VK_ACCOUNT__SUCCESS';
export const CREATE_VK_ACCOUNT__FAIL = 'CREATE_VK_ACCOUNT__FAIL';
export const UPDATE_VK_ACCOUNT = 'UPDATE_VK_ACCOUNT';
export const UPDATE_VK_ACCOUNT__SUCCESS = 'UPDATE_VK_ACCOUNT__SUCCESS';
export const UPDATE_VK_ACCOUNT__FAIL = 'UPDATE_VK_ACCOUNT__FAIL';
export const GET_FULL_VK_ACCOUNT = 'GET_FULL_VK_ACCOUNT';
export const GET_FULL_VK_ACCOUNT__SUCCESS = 'GET_FULL_VK_ACCOUNT_SUCCESS';
export const GET_FULL_VK_ACCOUNT__FAIL = 'GET_FULL_VK_ACCOUNT__FAIL';
export const DELETE_VK_ACCOUNT = 'DELETE_VK_ACCOUNT';
export const DELETE_VK_ACCOUNT__SUCCESS = 'DELETE_VK_ACCOUNT__SUCCESS';
export const DELETE_VK_ACCOUNT__FAIL = 'DELETE_VK_ACCOUNT__FAIL';
export const ACTIVATE_VK_ACCOUNT = 'ACTIVATE_VK_ACCOUNT';
export const ACTIVATE_VK_ACCOUNT__SUCCESS = 'ACTIVATE_VK_ACCOUNT__SUCCESS';
export const ACTIVATE_VK_ACCOUNT__FAIL = 'ACTIVATE_VK_ACCOUNT__FAIL';
export const CREATE_VK_TASK = 'CREATE_VK_TASK';
export const CREATE_VK_TASK__FAIL = 'CREATE_VK_TASK__FAIL';
export const CREATE_VK_TASK__SUCCESS = 'CREATE_VK_TASK__SUCCESS';
export const GET_VK_LOGS = 'GET_VK_LOGS';
export const GET_VK_LOGS__SUCCESS = 'GET_VK_LOGS__SUCCESS';
export const GET_VK_TASKS = 'GET_VK_TASKS';
export const GET_VK_TASKS__SUCCESS = 'GET_VK_TASKS__SUCCESS';
export const GET_VK_TASKS__FAIL = 'GET_VK_TASKS__FAIL';
export const DELETE_VK_TASK = 'DELETE_VK_TASK';
const initialState = {
    vkAccounts: {
        list: [],
        detailedList: {},
        isFetching: false,
        error: null,
    },
    vkAccountsCount: null,
    vkLogs: [],
    vkTasks: {
        list: [],
        isFetching: false,
        error: null,
    },
    newVkAccount: {
        isFetching: false,
        error: null,
        success: false,
    },
    updateVkAccount: {
        isFetching: false,
        success: false,
    },
    newVkTask: {
        isFetching: false,
        error: null,
    },
    isAccountActivationFetching: false,
};

export default createReducer(initialState, {
    [GET_VK_ACCOUNTS]: (state, action) => {
        return {
            ...state,
            vkAccounts: {
                ...initialState.vkAccounts,
                isFetching: true,
            },
            vkAccountsCount: null,
        }
    },
    [GET_VK_ACCOUNTS__SUCCESS]: (state, action) => {
        return {
            ...state,
            vkAccounts: {
                ...state.vkAccounts,
                isFetching: false,
                list: action.list,
            },
            vkAccountsCount: action.vkAccountsCount,
        }
    },
    [GET_VK_ACCOUNTS__FAIL]: (state, action) => {
        return {
            ...state,
            vkAccounts: {
                ...state.vkAccounts,
                error: action.error,
            },
            vkAccountsCount: null,
        }
    },
    [GET_FULL_VK_ACCOUNT__SUCCESS]: (state, action) => {
        return {
            ...state,
            vkAccounts: {
                ...state.vkAccounts,
                detailedList: {
                    ...state.vkAccounts.detailedList,
                    [action.id]: action.account,
                }
            }
        }
    },
    [DELETE_VK_ACCOUNT__SUCCESS]: (state, action) => {
        const newFullList = state.vkAccounts.detailedList;
        delete newFullList[action.id];
        const newList = state.vkAccounts.list.filter(account => account.id !== action.id);
        return {
            ...state,
            vkAccounts: {
                ...state.vkAccounts,
                detailedList: newFullList,
                list: newList,
            }
        }
    },
    [CREATE_VK_ACCOUNT]: (state, action) => {
        return {
            ...state,
            newVkAccount: {
                ...initialState.newVkAccount,
                isFetching: true,
            }
        }
    },
    [CREATE_VK_ACCOUNT__SUCCESS]: (state, action) => {
        return {
            ...state,
            vkAccounts: {
                list: [
                    ...state.vkAccounts.list,
                    action.account,
                ]
            },
            newVkAccount: {
                ...state.newVkAccount,
                isFetching: false,
                success: true,
            }
        }
    },
    [CREATE_VK_ACCOUNT__FAIL]: (state, action) => {
        return {
            ...state,
            newVkAccount: {
                ...state.newVkAccount,
                isFetching: false,
                error: action.error,
            }
        }
    },
    [GET_VK_LOGS__SUCCESS]: (state, action) => {
        return {
            ...state,
            vkLogs: action.logs,
        }
    },
    [CREATE_VK_TASK]: (state, action) => {
        return {
            ...state,
            newVkTask: {
                ...initialState.newVkTask,
                isFetching: true,
            }
        }
    },
    [CREATE_VK_TASK__SUCCESS]: (state, action) => {
        // TODO: add hot reload vk tasks (need task body from django)
        return {
            ...state,
            newVkTask: {
                ...state.newVkTask,
                isFetching: false,
            },
        }
    },
    [CREATE_VK_TASK__FAIL]: (state, action) => {
        return {
            ...state,
            newVkTask: {
                ...state.newVkTask,
                isFetching: false,
                error: action.error.detail,
            }
        }
    },
    [GET_VK_TASKS]: (state, action) => {
        return {
            ...state,
            vkTasks: {
                ...initialState.vkTasks,
                isFetching: true,
            }
        }
    },
    [GET_VK_TASKS__SUCCESS]: (state, action) => {
        return {
            ...state,
            vkTasks: {
                ...state.vkTasks,
                isFetching: false,
                list: action.tasks,
            }
        }
    },
    [GET_VK_TASKS__FAIL]: (state, action) => {
        return {
            ...state,
            vkTasks: {
                ...state.vkTasks,
                error: action.error,
            }
        }
    },
    [ACTIVATE_VK_ACCOUNT]: (state, action) => {
        return {
            ...state,
            vkAccounts: {
                ...state.vkAccounts,
                isFetching: true,
                isAccountActivationFetching: true
            }
        }
    },
    [ACTIVATE_VK_ACCOUNT__SUCCESS]: (state, action) => {
        return {
            ...state,
            isAccountActivationFetching: false,
        }
    },
    [ACTIVATE_VK_ACCOUNT__FAIL]: (state, action) => {
        return {
            ...state,
            isAccountActivationFetching: false,
        }
    },
    [DELETE_VK_TASK]: (state, action) => {
        return {
            ...state,
            vkTasks: {
                ...state.vkTasks,
                isFetching: true,
            }
        }
    },
    [UPDATE_VK_ACCOUNT]: (state, action) => {
        return {
            ...state,
            updateVkAccount: initialState.updateVkAccount,
        }
    },
    [UPDATE_VK_ACCOUNT__SUCCESS]: (state, action) => {
        const oldAccount = state.vkAccounts.list.find((acc) => acc.id === action.account.id);
        return {
            ...state,
            updateVkAccount: {
                ...state.updateVkAccount,
                success: true,
            },
            vkAccounts: {
                ...state.vkAccounts,
                list: [
                    ...state.vkAccounts.list.filter((acc) => acc.id !== action.account.id),
                    {
                        ...oldAccount,
                        ...action.account,
                    }
                ]
            }
        }
    },
    [UPDATE_VK_ACCOUNT__FAIL]: (state, action) => {
        return {
            ...state,
            updateVkAccount: {
                ...state.updateVkAccount,
                isFetching: false,
                success: false,
                error: action.error,
            }
        }
    }
})