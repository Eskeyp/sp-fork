import { createReducer } from '../'

export const SEND_REGISTER_DATA  = 'SEND_REGISTER_DATA';
export const SEND_REGISTER_DATA__SUCCESS = 'SEND_REGISTER_DATA__SUCCESS';
export const SEND_REGISTER_DATA__FAILED = 'SEND_REGISTER_DATA__FAILED';
export const SEND_LOGIN_DATA = 'SEND_LOGIN_DATA';
export const SEND_LOGIN_DATA__SUCCESS = 'SEND_LOGIN_DATA__SUCCESS';
export const SEND_LOGIN_DATA__FAILED = 'SEND_LOGIN_DATA__FAILED';

const initialState = {
    register: {
        error: null,
        isFetching: false,
        success: false,
    },
    login: {
        error: null,
        isFetching: false,
        success: false,
    },
    token: null,
};

export default createReducer(initialState, {
    [SEND_REGISTER_DATA]: (state, action) => {
        return {
            ...state,
            register: {
                ...state.register,
                error: null,
                success: false,
                isFetching: true,
            },
        }
    },
    [SEND_REGISTER_DATA__SUCCESS]: (state, action) => {
        return {
            ...state,
            register: {
                ...state.register,
                isFetching: false,
                success: true,
            }
        }
    },
    [SEND_REGISTER_DATA__FAILED]: (state, action) => {
        return {
            ...state,
            register: {
                ...state.register,
                isFetching: false,
                error: action.error,
            }
        }
    },
    [SEND_LOGIN_DATA]: (state, action) => {
        return {
            ...state,
            login: {
                ...state.login,
                error: null,
                success: null,
                isFetching: true,
            }
        }
    },
    [SEND_LOGIN_DATA__SUCCESS]: (state, action) => {
        return {
            ...state,
            login: {
                ...state.login,
                isFetching: false,
                success: true,
            }
        }
    },
    [SEND_LOGIN_DATA__FAILED]: (state, action) => {
        return {
            ...state,
            login: {
                ...state.login,
                isFetching: false,
                error: action.error,
            }
        }
    }
})