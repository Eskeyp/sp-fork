import {
    applyMiddleware,
    createStore,
    combineReducers,
} from 'redux';
import thunk from 'redux-thunk';
import { routerReducer, routerMiddleware } from 'react-router-redux'
import { browserHistory } from 'react-router';
import reducers from './reducers';

export const API_URL = process.env.NODE_ENV === 'production' && 'http://smmrevolution.ru/api' ||
  process.env.NODE_ENV === 'staging' && 'http://178.62.59.15/api' || 'http://localhost/api';
// export const API_URL = 'http://localhost/api';
export const createMainStore = (initialState, reducers) => {
    const mainReducers = combineReducers(reducers);
    const router = routerMiddleware(browserHistory);
    return createStore(mainReducers, initialState, applyMiddleware(thunk, router));
};
export function createReducer(initialState, actionTypesMap) {
    return (state = initialState, action) => {
        if (typeof actionTypesMap[action.type] === 'function') {
            return actionTypesMap[action.type](state, action)
        }
        return state;
    }
}
export default createMainStore({}, {
    ...reducers,
    routing: routerReducer
})
