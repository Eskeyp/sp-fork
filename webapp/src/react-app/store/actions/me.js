import { push } from 'react-router-redux'
import { API_URL } from '../'
import * as constants from "../reducers/me";
import { createNotNullObject } from '../../helpers/';
import {post, get, deleteRequest, put } from "../../helpers/api";

export function getVkAccounts() {
    return async dispatch => {
        dispatch({
            type: constants.GET_VK_ACCOUNTS,
        });
        try {
            const response = await get(`${API_URL}/account/vk/list/`);
            const vkAccountsCount = response.reduce((acc, curr) => {
                const count = acc[curr.status] ? acc[curr.status] : 0;
                return {
                    ...acc,
                    [curr.status]: count + 1,
                }
            }, {});
            return dispatch({
                type: constants.GET_VK_ACCOUNTS__SUCCESS,
                list: response,
                vkAccountsCount,
            })
        } catch (error) {
            return dispatch({
                type: constants.GET_VK_ACCOUNTS__FAIL,
                error,
            })
        }
    }
}

export function createVkAccount(login, password, phone, proxy) {
    return async dispatch => {
        dispatch({
            type: constants.CREATE_VK_ACCOUNT,
        });
        const requestData = createNotNullObject({login, password, phone, proxy});
        try {
            const response = await post(`${API_URL}/account/vk/create/`, requestData);
            return dispatch({
                type: constants.CREATE_VK_ACCOUNT__SUCCESS,
                account: {
                    id: response.id,
                    status: response.status,
                    status_id: response.status_id,
                        ...requestData,
                },
            })
        } catch (error) {
            return dispatch({
                type: constants.CREATE_VK_ACCOUNT__FAIL,
                error,
            })
        }
    }
}
export function updateVkAccount(id, login, password, phone, proxy) {
    return async dispatch => {
        dispatch({
            type: constants.UPDATE_VK_ACCOUNT,
        });
        const requestData = createNotNullObject({login, password, phone, proxy});
        try {
            const response = await put(`${API_URL}/account/vk/update/${id}/`, requestData);
            console.log(response);
            return dispatch({
                type: constants.UPDATE_VK_ACCOUNT__SUCCESS,
                account: response,
            })
        } catch (error) {
            return dispatch({
                type: constants.UPDATE_VK_ACCOUNT__FAIL,
                error,
            })
        }
    }
}
export function getFullVkAccount(id) {
    return async dispatch => {
        dispatch({
            type: constants.GET_FULL_VK_ACCOUNT,
        });
        try {
            const response = await get(`${API_URL}/account/vk/detail/${id}/`);
            return dispatch({
                type: constants.GET_FULL_VK_ACCOUNT__SUCCESS,
                id,
                account: response,
            })
        } catch (error) {
            return dispatch({
                type: constants.GET_VK_ACCOUNTS__FAIL,
                error,
            })
        }
    }
}

export function deleteVkAccount(id) {
    return async dispatch => {
        dispatch({
            type: constants.DELETE_VK_ACCOUNT,
        });
        try {
            const response = await deleteRequest(`${API_URL}/account/vk/delete/${id}`);
            return dispatch({
                type: constants.DELETE_VK_ACCOUNT__SUCCESS,
                id,
            })
        } catch (error) {
            return dispatch({
                type: constants.DELETE_VK_ACCOUNT__FAIL,
            })
        }
    }
}

export function activateVkAccount(id) {
    return async dispatch => {
        try {
            dispatch({
                type: constants.ACTIVATE_VK_ACCOUNT
            });
            const requestData = { account_id: id };
            const response = await post(`${API_URL}/account/vk/activate/`, requestData);
            await dispatch(getVkAccounts());
            dispatch({
                type: constants.ACTIVATE_VK_ACCOUNT__SUCCESS
            });

        } catch (error) {
            await dispatch(getVkAccounts());
            dispatch({
                type: constants.ACTIVATE_VK_ACCOUNT__FAIL,
            });
            console.log(error)
        }
    }
}

export function deactivateVkAccount(id) {
    return async dispatch => {
        try {
            const requestData = { account_id: id };
            const response = await post(`${API_URL}/account/vk/deactivate/`, requestData);
            dispatch(getVkAccounts());
        } catch (error) {
            dispatch(getVkAccounts());
            console.log(error)
        }
    }
}

export function createTask(account_id, name) {
    return async dispatch => {
        dispatch({
            type: constants.CREATE_VK_TASK,
        });
        try {
            const requestData = { task_name: name, account_id: account_id};
            const response = await post(`${API_URL}/task/vk/create/`, requestData);
            dispatch({
                type: constants.CREATE_VK_TASK__SUCCESS,
            });
            dispatch(getVkTasks());
            dispatch(push('/me/my-tasks'));
        } catch (err) {
            const error = await err;
            dispatch({
                type: constants.CREATE_VK_TASK__FAIL,
                error,
            })
        }
    }
}
export function getVkTasks() {
    return async dispatch => {
        dispatch({
            type: constants.GET_VK_TASKS,
        })
        try {
            const response = await get(`${API_URL}/task/vk/list/`);
            dispatch({
                type: constants.GET_VK_TASKS__SUCCESS,
                tasks: response,
            })
        } catch (err) {
            const error = await err;
            dispatch({
                type: constants.GET_VK_ACCOUNTS__FAIL,
                error,
            })
        }
    }
}
export function getVkLogs() {
    return async dispatch => {
        try {
            const response = await get(`${API_URL}/account/vk/logs/`);
            dispatch({
                type: constants.GET_VK_LOGS__SUCCESS,
                logs: response,
            });
            //dispatch(push('/me/vk/tasks'));
        } catch (err) {
            console.log(err);
        }
    }
}

export function activateVkTask(task_id) {
    return async dispatch => {
        try {
            dispatch({type: constants.DELETE_VK_TASK});
            const response = await post(`${API_URL}/task/vk/activate/`, {task_id});
            await dispatch(getVkTasks());
        }
        catch (err) {
            console.log(err)
        }
    }
}

export function deactivateVkTask(task_id) {
    return async dispatch => {
        try {
            dispatch({type: constants.DELETE_VK_TASK});
            const response = await post(`${API_URL}/task/vk/deactivate/`, {task_id});
            await dispatch(getVkTasks());
        }
        catch (err) {
            console.log(err)
        }
    }
}

export function deleteVkTask(id) {
    return async dispatch => {
        try {
            dispatch({type: constants.DELETE_VK_TASK});
            const response = await deleteRequest(`${API_URL}/task/vk/delete/${id}`);
            await dispatch(getVkTasks());
        } catch (err) {
            console.log(err)
        }
    }
}
