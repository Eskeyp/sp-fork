import { post } from '../../helpers/api';
import { API_URL } from '../'
import * as constants from '../reducers/auth';
import { push } from 'react-router-redux'

export function sendRegisterData(email, password, confirm_password) {
    return async dispatch => {
        dispatch({
            type: constants.SEND_REGISTER_DATA,
        });
        const requestData = {
            email,
            password,
            confirm_password: password,
        };
        if (!email || !password || !confirm_password) {
            return dispatch({
                type: constants.SEND_REGISTER_DATA__FAILED,
                error: {
                    common: 'Заполните все поля!',
                },
            })

        }
        if (password !== confirm_password) {
            return dispatch({
                type: constants.SEND_REGISTER_DATA__FAILED,
                error: {
                    common:'Пароли не совпадают!'
                },
            })
        }
        try {
            const response = await post(`${API_URL}/users/create/`, requestData);
            dispatch(push('/register/email'));
            return dispatch({
                type: constants.SEND_REGISTER_DATA__SUCCESS,
                token: response.token,
            })
        } catch (err) {
            const error = await err;
            return dispatch({
                type: constants.SEND_REGISTER_DATA__FAILED,
                error,
            })
        }
    }
}

export function sendLoginData(email, password) {
    return async dispatch => {
        dispatch({
            type: constants.SEND_LOGIN_DATA,
        });
        const requestData = {
            email,
            password,
        };
        if (!email || !password) {
            return dispatch({
                type: constants.SEND_LOGIN_DATA__FAILED,
                error: {
                    common: 'Заполните все поля!',
                }
            })
        }
        try {
            const response = await post(`${API_URL}/users/login/`, requestData);
            dispatch({
                type: constants.SEND_LOGIN_DATA__SUCCESS,
                token: response.token,
            });
            dispatch(push('/me/accounts'));
        } catch (err) {
            const error = await err;
            return dispatch({
                type: constants.SEND_LOGIN_DATA__FAILED,
                error,
            })
        }
    }
}
