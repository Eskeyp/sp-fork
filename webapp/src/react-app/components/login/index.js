import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router'
import { connect } from 'react-redux'
import Loading from '../common/loading'
import { sendLoginData } from '../../store/actions/auth';
import Tooltip from 'rc-tooltip';
const Input = ({error, inputProps }) => (
    <Tooltip overlay={error || ''} visible={!!error} placement="top">
        <input {...inputProps} data-tip={error}/>
    </Tooltip>
)
class Login extends Component {
    constructor(args) {
        super(args);
        this.state = {
            login: '',
            password: '',
            showErrors: false,
        };
        this.errorTimer = null;
    }
    onEdit = (label) => (e) => {
        this.setState({ [label]: e.target.value })
    };
    onSubmit = (e) => {
        e.preventDefault();
        this.setState({showErrors: true});
        clearTimeout(this.errorTimer);
        this.errorTimer = setTimeout(() => this.setState({showErrors: false}), 2000);
        this.props.sendLoginData(this.state.login, this.state.password);
    };
    render() {
        const { error, isFetching } = this.props;
        if (isFetching) {
            return (
                <Loading />
            )
        }
        return (
            <div className="login-page">
                <div className="login-page__content">
                    <div className="login-page__logo">
                        SMMREVOLUTION
                    </div>
                    <form onSubmit={this.onSubmit} className="login-page__form login-page-form">
                        <Input
                            inputProps={{
                                type: "text",
                                className: "input input_text input_block login-page-form__input",
                                placeholder: "Логин",
                                onChange: this.onEdit('login'),
                                value: this.state.login,
                            }}
                            error={this.state.showErrors && error && error.email && Array.isArray(error.email) ? error.email[0] : null}
                        />
                        <Input
                            inputProps={{
                                type: "password",
                                className: "input input_text input_block login-page-form__input",
                                placeholder: "Пароль",
                                onChange: this.onEdit('password'),
                                value:this.state.password,
                            }}
                            error={this.state.showErrors && error && error.password && Array.isArray(error.password) ? error.password[0] : null}
                        />
                        <button
                            type="submit"
                            className="button button_block-100"
                        >
                            Войти
                        </button>
                    </form>
                    <div className="login-page__have-account">
                        <Link
                            to="/register"
                            className="link login-page__have-account-link"
                        >
                            Еще нет аккаунта?
                        </Link>
                    </div>
                    {/*{error && error.common &&*/}
                    {/*<div className="login-page__error-block">*/}
                        {/*{error.common}*/}
                    {/*</div>*/}
                    {/*}*/}
                </div>
            </div>
        )
    }
}

Login.propTypes = {
    isFetching: PropTypes.bool,
    error: PropTypes.shape({}),
    success: PropTypes.bool,
    sendLoginData: PropTypes.func,
};

const mapStateToProps = (state) => ({
    isFetching: state.auth.login.isFetching,
    error: state.auth.login.error,
    success: state.auth.login.success,
});

export default connect(mapStateToProps, { sendLoginData })(Login);
