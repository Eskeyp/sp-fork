import React, { Component } from 'react';
import {Link} from 'react-router'

class Success extends Component {
    render() {
        return (
            <div className="success-register-page">
                <div className="success-register-page__content">
                    <div className="success-register-page__logo">
                        VKJUST
                    </div>
                    <div className="success-register-page__text">
                        Вы успешно зарегистрировались. Для продолжения регистрации пройдите по ссылке, отправленной вам на E-Mail.
                        <div className="success-register-page__links">
                            <Link to={"/login"} className="link">Авторизация</Link>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Success
