import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router'
import PropTypes from 'prop-types';
import Loading from '../common/loading'
import { sendRegisterData } from '../../store/actions/auth';
import Tooltip from 'rc-tooltip';
const Input = ({error, inputProps }) => (
    <Tooltip overlay={error || ''} visible={!!error} placement="top">
        <input {...inputProps} data-tip={error}/>
    </Tooltip>
)

class Register extends Component {
    constructor(args) {
        super(args)
        this.state = {
            email: '',
            password: '',
            confirm_password: '',
            showErrors: false,
        };
        this.errorTimer = null;
    }
    onEdit = (label) => (e) => {
        this.setState({ [label]: e.target.value })
    }
    onSubmit = (e) => {
        e.preventDefault();
        const {
            email,
            password,
            confirm_password
        } = this.state;
        this.props.sendRegisterData(email, password, confirm_password);
        this.setState({showErrors: true});
        clearTimeout(this.errorTimer);
        this.errorTimer = setTimeout(() => this.setState({showErrors: false}), 2000);
        this.props.sendLoginData(this.state.login, this.state.password);
    };
    render() {
        const {
            isFetching,
            error,
        } = this.props;
        if (isFetching) {
            return <Loading />
        }
        return (
            <div className="login-page">
                <div className="login-page__content">
                    <div className="login-page__logo">
                        SMMREVOLUTION
                    </div>
                    <form onSubmit={this.onSubmit} className="login-page__form login-page-form">
                        <Input
                            inputProps={{
                            type: "text",
                            className: "input input_text input_block login-page-form__input",
                            placeholder: "E-Mail",
                            onChange: this.onEdit('email'),
                            value: this.state.email,
                        }}
                            error={this.state.showErrors && error && error.email && Array.isArray(error.email) ? error.email[0] : null}
                        />
                        <Input
                            inputProps={{
                                type: "password",
                                className: "input input_text input_block login-page-form__input",
                                placeholder: "Пароль",
                                onChange: this.onEdit('password'),
                                value: this.state.password
                            }}
                            error={this.state.showErrors && error && error.password && Array.isArray(error.password) ? error.password[0] : null}
                        />
                        <Input
                            inputProps={{
                                type: "password",
                                className: "input input_text input_block login-page-form__input",
                                placeholder: "Пароль (еще раз)",
                                onChange: this.onEdit('confirm_password'),
                                value: this.state.confirm_password
                            }}
                        />
                        <button
                            type="submit"
                            className="button button_block-100"
                        >
                            Регистрация
                        </button>
                    </form>
                    <div className="login-page__have-account">
                        <Link
                            to="/login"
                            className="link login-page__have-account-link"
                        >
                            Уже есть аккаунт?
                        </Link>
                    </div>
                    {error && error.common &&
                    <div className="login-page__error-block">
                        {error.common}
                    </div>
                    }
                </div>
            </div>
        )
    }
}
const mapStateToProps = (state) => ({
    isFetching: state.auth.register.isFetching,
    error: state.auth.register.error,
    success: state.auth.register.success,
});
Register.propTypes = {
    isFetching: PropTypes.bool,
    error: PropTypes.shape({}),
    success: PropTypes.bool,
    sendRegisterData: PropTypes.func,
};
export default connect(mapStateToProps, { sendRegisterData })(Register)
