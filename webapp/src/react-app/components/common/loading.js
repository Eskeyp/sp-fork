import React from 'react';
import ReactLoading from 'react-loading';

const Loading = () => (
    <ReactLoading className ='page-loader' type='spin' color='#96aac2'/>
);

export default Loading;
