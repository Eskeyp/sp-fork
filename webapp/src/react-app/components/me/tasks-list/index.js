import React, { Component } from 'react'
import { connect } from 'react-redux'
import Task from './task'
import Loading from '../../common/loading'
import { deleteVkTask, activateVkTask, deactivateVkTask } from '../../../store/actions/me'
import CreateTaskPage from '../create-task'
class TasksList extends Component {
    constructor(args) {
        super(args);
        this.state = {
            isVisibleModal: false,
        }
    }
    deleteTask = (id) => {
        this.props.deleteVkTask(id);
    }
    activateTask = (id) => {
        this.props.activateVkTask(id);
    }
    deactivateTask = (id) => {
        this.props.deactivateVkTask(id);
    }
    render() {
        const { list, isFetching } = this.props;
        if (isFetching) {
            return <Loading />
        }

        return (
          <div className="tasks">
              <div className="tasks__content">
                {list.length !== 0 &&
                      <button className="create btn btn-success"
                          onClick={(e) => {
                              e.preventDefault();
                              this.setState({isVisibleModal: true});
                          }}
                      >
                          +
                      </button>
                  }
                  {list.map((task) => (
                      <Task
                          task={task}
                          deleteTask={this.deleteTask}
                          activateTask={this.activateTask}
                          deactivateTask={this.deactivateTask}
                      />
                  ))}
              </div>
              {list && list.length === 0  && !isFetching && this.props.accounts && this.props.accounts.length > 0 &&
                <div className="no-account">
                    <div className="no-account__text">
                        Пожалуйста, добавьте задачу,<br/> чтобы начать работу...
                    </div>
                    <div className="no-account__buttons">
                        <button
                            className="button no-account__button"
                            onClick={() => this.setState({isVisibleModal: true})}
                        >
                            Добавить задачу
                        </button>
                    </div>
                </div>
              }
              {this.state.isVisibleModal &&
                <CreateTaskPage
                    isVisible={this.state.isVisibleModal}
                    setInvisibleCreateTaskModal={() => this.setState({isVisibleModal: false})}
                />
              }

          </div>
        )
    }
}

const mapStateToProps = (state) => ({
    list: state.me.vkTasks.list,
    isFetching: state.me.vkTasks.isFetching,
    accounts: state.me.vkAccounts.list,
});
export default connect(mapStateToProps, {deleteVkTask, activateVkTask, deactivateVkTask})(TasksList)
