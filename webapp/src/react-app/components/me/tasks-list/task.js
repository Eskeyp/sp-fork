import React, { Component } from 'react'
import task_img from '../../../../static/task_icon.png'

class Task extends Component {
    constructor(args) {
        super(args);
        this.state = {
            showTaskNav: false,
            showTaskStatus: false,
        }
    }
    render() {
        const { task } = this.props;
        var visibleNav;
        var visibleStatus;
        if (this.state.showTaskNav)  { visibleNav = false;}
        if (!this.state.showTaskNav)  { visibleNav = true; }
        if (this.state.showTaskStatus)  { visibleStatus = false;}
        if (!this.state.showTaskStatus)  { visibleStatus = true; }
        return (
          <div className="task">
              {this.state.showTaskNav &&
              <div className="task__nav">
                  <ul>
                  <li><a href="#" onClick={() => this.props.deleteTask(task.id)}>Удалить</a></li>
                  </ul>
              </div>
              }
              <div className="task__content">
                <div className="task__nav_button">
                    <a href="#" onClick={() => this.setState({showTaskNav: visibleNav})}>
                        <i className="fas fa-ellipsis-v"></i>
                    </a>
                </div>

                <div className="task__title">
                    {task.name}
                </div>
                <div className="task__subtitle">
                    Добавляться в друзья
                </div>
                <div className="task__status">
                    {this.state.showTaskStatus &&
                    <div className="task__status__nav">
                        <ul>
                        {task.active &&
                        <li><a href="#" >Удалить</a></li>&&
                        <li><a href="#" onClick={() => this.props.deactivateTask(task.id)} >Остановить</a></li>
                        }
                        {!task.active && <li><a href="#" onClick={() => this.props.activateTask(task.id)}>Активировать</a></li>}
                        </ul>
                    </div>
                    }
                    {!task.active && <span className="pause" onClick={() => this.setState({showTaskStatus: visibleStatus})}>На паузе</span>}
                    {task.active && <span className="active"onClick={() => this.setState({showTaskStatus: visibleStatus})}>Выполняется</span>}
                </div>
                <div className="task__image">
                    <div className="task__online"></div>
                    <div className="task__offline"></div>
                    <img src={task.account_image} />
                </div>
              </div>
          </div>

        )
    }

}

export default Task
