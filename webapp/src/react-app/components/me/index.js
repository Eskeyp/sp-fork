import React, {Component} from 'react'
import {connect} from 'react-redux'
import { Link } from 'react-router'
import { getVkAccounts, getVkTasks } from '../../store/actions/me'
import NoAccount from './no-account'
import Sidebar from './sidebar'
import MainMenu from './main-menu'
class Root extends Component {
    componentDidMount() {
        this.props.getVkAccounts();
        this.props.getVkTasks();
    }
    render() {
        return (
            <div className="me-page">
                <div className="me-page__content">

                    <div className="me-page__page page-content">
                        <MainMenu/>
                        <div className="me-page__center main-block">
                            <div className="main-block__left-menu left-menu-main">
                                <div className="left-menu-main__heading">
                                    Продвижение
                                </div>
                                <div className="left-menu-main__menu">
                                    <div className="left-menu-main__menu-item">

                                    </div>
                                    <div className="left-menu-main__menu-item">
                                        <Link
                                            to="/me/accounts"
                                            className="left-menu-main__menu-link"
                                            activeClassName="left-menu-main__menu-link left-menu-main__menu-link_active"
                                        >
                                            Аккаунты
                                        </Link>
                                    </div>
                                    <div className="left-menu-main__menu-item">

                                        <Link
                                            to="/me/my-tasks"
                                            className="left-menu-main__menu-link"
                                            activeClassName="left-menu-main__menu-link left-menu-main__menu-link_active"
                                        >
                                            Задачи
                                        </Link>

                                    </div>
                                </div>
                            </div>
                            <div className="main-menu__content">
                                {this.props.children}
                                {!this.props.isFetching && (!this.props.accounts || !this.props.accounts.length) &&
                                <NoAccount/>}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
const mapStateToProps = (state) => ({
    tasks: state.me.vkTasks.list,
    accounts: state.me.vkAccounts.list,
    isFetching: state.me.vkAccounts.isFetching,
});
export default connect(mapStateToProps, { getVkAccounts, getVkTasks })(Root)
