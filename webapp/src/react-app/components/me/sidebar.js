import React, { Component } from 'react'
import vk from '../../../static/vk.png'

class Sidebar extends Component {
    render() {
        return (
            <div className="sidebar">
                <ul className="sidebar__menu">
                    <li className="sidebar__item sidebar__item_plus">
                        <a href="#" className="sidebar__link sidebar__link_plus">+</a>
                    </li>
                    <li className="sidebar__item">
                        <a href="#" className="sidebar__link">
                            <img className="sidebar__icon" src={vk}/>
                        </a>
                    </li>
                </ul>
            </div>
        )
    }
}
export default Sidebar