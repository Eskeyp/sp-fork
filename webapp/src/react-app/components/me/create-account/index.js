import React, { Component } from 'react'
import { connect } from 'react-redux'
import CreateAccountForm from './create-account-form'
import { createVkAccount } from '../../../store/actions/me'

class CreateAccountPage extends Component {
    onSubmit = (login, password, phone, proxy) => {
        this.props.createVkAccount(login, password, phone, proxy)
    };
    componentWillReceiveProps(nextProps) {
        if (nextProps.success && !this.props.success) {
            this.props.setInvisible();
        }
    }
    render() {
        const { error, isFetching, isVisible } =     this.props;
        if (!isVisible) return null;
        if (isVisible) return ([
            <div className="full-page-shadow" />,
            <div className="create-account-modal">
                <div className="create-account-modal__content">
                    {!isFetching &&
                    <CreateAccountForm
                        onSubmit={this.onSubmit}
                        closeForm={this.props.setInvisible}
                    />
                    }
                    {isFetching && 'loading...'}
                    {error && <div> {error}</div> }
                </div>
            </div>
        ])
    }
}

const mapStateToProps = (state) => ({
    isFetching: state.me.newVkAccount.isFetching,
    error: state.me.newVkAccount.error,
    success: state.me.newVkAccount.success,
});
export default connect(mapStateToProps, { createVkAccount })(CreateAccountPage)