import React, { Component } from 'react'

class CreateAccountForm extends Component {
    constructor(args) {
        super(args);
        this.state = {
            login: '',
            password: '',
            phone: '',
            proxy: '',
        }
    }

    componentDidMount() {
        const state = {};
        if (this.props.login) {
            state['login'] = this.props.login;
        }
        if (this.props.password) {
            state['password'] = this.props.password;
        }
        if (this.props.phone) {
            state['phone'] = this.props.phone;
        }
        if (this.props.proxy) {
            state['proxy'] = this.props.proxy;
        }
        if (Object.keys(state).length > 0) {
            this.setState(state)
        }
    }
    onEdit = (label) => (e) => {
        this.setState({ [label]: e.target.value })
    };
    render() {
        const { login, password, phone, proxy } = this.state;
        return (
            <form
                onSubmit={(e) => {e.preventDefault(); this.props.onSubmit(login, password, phone, proxy)}}
                className="create-account-modal__form"
            >
                <a
                    href="#"
                    className="create-account-modal__close-button"
                    onClick={(e) => {
                        e.preventDefault();
                        this.props.closeForm();
                    }}
                >
                    ✕
                </a>
                <input
                    type="text"
                    className="input input_text input_block create-account-modal__input"
                    value={login}
                    required
                    onChange={this.onEdit('login')}
                    placeholder="Логин"
                />
                <input
                    type="text"
                    className="input input_text input_block create-account-modal__input"
                    value={password}
                    required
                    onChange={this.onEdit('password')}
                    placeholder="Пароль"
                />
                
                <button
                    type="submit"
                    className="button create-account-modal__button"
                >
                    {this.props.isEditForm && 'Изменить' || 'Добавить аккаунт'}
                </button>
            </form>
        )
    }
}

export default CreateAccountForm
