import React, {Component} from 'react';
import CreateAccountModal from '../create-account';

class NoAccount extends Component {
    constructor(args) {
        super(args);
        this.state = {
            createAccountModalShow: false,
        }
    }
    render() {
      return ([
      <div className="no-account">
          <div className="no-account__text">
              Пожалуйста, добавьте первый аккаунт,<br/> чтобы начать работу...
          </div>
          <div className="no-account__buttons">
              <button
                  className="button no-account__button"
                  onClick={() => this.setState({createAccountModalShow: true})}
              >
                  Добавить аккаунт
              </button>
          </div>
      </div>,
      <CreateAccountModal
          isVisible={this.state.createAccountModalShow}
          setInvisible={() => this.setState({createAccountModalShow: false})}
      />

    ])
  }
}
export default NoAccount
