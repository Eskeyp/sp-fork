import React, { Component } from 'react'
import classNames from 'classnames'

class AccountCirlce extends Component {
  render() {
    return (
      <div
        className={classNames("account-circle", {
          'account-circle_selected': this.props.selected,
        })}
        onClick={this.props.selectAccount}
      >
        <img
          className={classNames("account-circle__image", {
            'account-circle__image_empty': !this.props.image,
        })}
          src={this.props.image}/>
        <div className="account-circle__details">
        <div className="account-cirlce__name">
          {this.props.name}
        </div>
          {this.props.id &&
            <div className="account-circle__id">ID {this.props.id}</div>
          }
        </div>
      </div>
    )
  }
}

export default AccountCirlce