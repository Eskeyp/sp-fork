import React, { Component } from 'react'
import { connect } from 'react-redux'
import CreateTaskForm from './create-task-form'
import { createTask } from '../../../store/actions/me'

class CreateTaskPage extends Component {
    onSubmit = (account, name) => {
        this.props.createTask(account, name)
    };
    render() {
        const { error, isFetching, isVisible } =     this.props;
        if (!isVisible) return null;

        if (isVisible) return ([
            <div className="full-page-shadow" />,
            <div className="create-account-modal">
                <div className="create-account-modal__content task_add">
                  <CreateTaskForm
                      accounts={this.props.vkAccounts}
                      onSubmit={this.onSubmit}
                      closeForm={this.props.setInvisibleCreateTaskModal}
                      error={this.props.error}
                  />
                </div>
            </div>
        ])
    }

}

const mapStateToProps = (state) => ({
    vkAccounts: state.me.vkAccounts.list,
    error: state.me.newVkTask.error,
});
export default connect(mapStateToProps, { createTask })(CreateTaskPage)
