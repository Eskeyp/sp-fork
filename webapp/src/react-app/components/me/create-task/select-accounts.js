import React, { Component } from 'react'
import AccountCircle from './account-circle'

class SelectAccounts extends Component {
  render() {
    console.log(this.props.accounts, 'XXX');
    return (
      <div className="select-accounts">
        {this.props.accounts.map(account => (
          <AccountCircle
            selected={this.props.selectedAccount === account.id}
            name={account.login}
            selectAccount={() => this.props.selectAccount(account.id)}
            image={account.avatar}
          />
        ))}
      </div>
    )
  }
}

export default SelectAccounts;