import React, { Component } from 'react'
import SelectAccounts from './select-accounts'
class CreateTaskForm extends Component {
    constructor(args) {
        super(args);
        this.state = {
            account: null,
            name: '',
        }
    }
    onEdit = (label) => (e) => {
        this.setState({ [label]: e.target.value })
    };
    selectAccount = (id) => {
      this.setState({account: id})
    }
    render() {
        const { accounts, error } = this.props;
        const { account } = this.state;
        if (this.props.accounts.length === 0) {
            return (
                <form
                    className="create-account-modal__form"
                    >
                    <a
                        href="#"
                        className="create-account-modal__close-button"
                        onClick={(e) => {
                            e.preventDefault();
                            this.props.closeForm();
                        }}
                    >
                        ✕
                    </a>
                    <span className="create-account-modal__error">
                        <i className="fas fa-exclamation-triangle"/>
                        Чтобы создать задачу, нужно добавить и активировать аккаунт
                    </span>
                </form>
            )
        }
        const value = this.state.account === '' ? accounts[0].id : this.state.account;

        return (
            <form
                className="create-account-modal__form"
                onSubmit={(e) => {
                    e.preventDefault();
                    const value = this.state.account === '' ? accounts[0].id : this.state.account;
                    this.props.onSubmit(value, this.state.name);
                    if (!{error}) {
                      console.log({error})
                      this.props.closeForm();
                    }
            }}>
                <a
                    href="#"
                    className="create-account-modal__close-button"
                    onClick={(e) => {
                        e.preventDefault();
                        this.props.closeForm();
                    }}
                >
                    ✕
                </a>
                <div className="section section_underlined">
                  <div className="section__content">
                      <input
                          type="text"
                          className="input input_text input_block create-account-modal__input"
                          value={this.state.name}
                          onChange={this.onEdit('name')}
                          placeholder="Название задачи"
                          required
                      />
                      <label className="info">Выберите тип задачи и действие.</label>
                  </div>
                </div>
                <div className="section section_underlined section_no-padding">
                  <div className="section__line">
                    <div className="section__number">1</div>
                    <div className="section__title">Аккаунты</div>
                  </div>
                  <div className="section__content">
                      {/*<select*/}
                          {/*className="section__select create-account-modal__select select "*/}
                          {/*onChange={this.onEdit('account')}*/}
                          {/*value={account}*/}
                      {/*>*/}
                          {/*{accounts.map(account =>*/}
                              {/*<option key={account.id} value={account.id}>{account.login}</option>)}*/}
                      {/*</select>*/}
                      <SelectAccounts
                        accounts={accounts}
                        selectAccount={this.selectAccount}
                        selectedAccount={this.state.account}
                      />
                  </div>
                </div>


                {/*<div className="section">
                    <div className="section__number">1</div>
                    <label className="section__title">С чем будем работать ?</label>
                    <div className="section__options">
                        <div className="section__options__name">
                          Личная страница
                        </div>
                    </div>
                </div>*/}
                <div className="section section_dark section_no-padding">
                  <div className="section__content">
                    <div className="section__controls">
                      <button className="create-account-modal__button create-account-modal__button_second create-account-modal__button_pink">
                        Создать задачу
                      </button>
                    </div>
                    {error &&
                    <div className="section__error">
                      <span className="create-account-modal__error">
                        <i className="fas fa-exclamation-triangle"/> {error}
                      </span>
                    </div>}
                  </div>
                </div>
                {/*<button*/}
                    {/*type="submit"*/}
                    {/*className="button create-account-modal__button"*/}
                {/*>*/}
                    {/*Создать задачу*/}
                {/*</button>*/}

            </form>
        )
    }
}

export default CreateTaskForm
