import React, { Component } from 'react'
import classNames from "classnames";
import {connect} from 'react-redux'
import { Link } from 'react-router'

class MainMenu extends Component {
    render() {
        const isAccountPage = this.props.pathname.search('/me/accounts') !== -1
        return (
            <div className="main-menu">
                <ul className="main-menu__list">
                    <li className="main-menu__item">
                        <Link to="/"  className="main-menu__link main-menu__link_logo">SMMREVOLUTION</Link>
                    </li>
                    {isAccountPage &&
                    <li className="main-menu__item">
                        <ul className="statuses-list">
                            <Link
                                exact
                                to="/me/accounts?filter=all"
                                className={classNames("statuses-list__status", {
                                    "statuses-list__status_active": !this.props.query || !this.props.query['filter'],
                                })}
                                activeClassName="statuses-list__status statuses-list__status_active">
                                Все
                            </Link>
                            {this.props.vkAccountsCount && Object.keys(this.props.vkAccountsCount).map((filter, idx) =>
                                <Link
                                    to={`/me/accounts?filter=${idx}`}
                                    className="statuses-list__status"
                                    activeClassName="statuses-list__status statuses-list__status_active"
                                >
                                    {filter} {this.props.vkAccountsCount[filter]}
                                </Link>
                            )}
                        </ul>
                    </li>
                    }
                </ul>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    vkAccountsCount: state.me.vkAccountsCount,
    pathname: state.routing.locationBeforeTransitions.pathname,
    query: state.routing.locationBeforeTransitions.query,
});
export default connect(mapStateToProps)(MainMenu)
