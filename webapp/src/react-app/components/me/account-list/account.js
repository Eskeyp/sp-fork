import React, { Component } from 'react'
import { Link } from 'react-router'
class AccountDetail extends Component {
    render() {
        const { account } = this.props;
        return (
            <tr>
                <td>{account.login}</td>
                <td>{account.status}</td>
                <td>
                    <div className="control">
                        <ul>
                            <li><a className="btn btn-danger" href="#" onClick={(e) => {
                                e.preventDefault();
                                this.props.onDelete(account.id);
                            }}>
                                <i className="far fa-trash-alt"></i>
                            </a></li>
                        
                            {((account.status_id === "0" || account.status_id === "1" || account.status_id === "3") && <li>
                                <a className="btn btn-success" href="#" onClick={(e) => {
                                    e.preventDefault();
                                    this.props.onActivate(account.id);
                                }}>
                                    Старт
                                </a></li>) ||
                            <li>
                                <a className="btn btn-success" href="#" onClick={(e) => {
                                    e.preventDefault();
                                    this.props.onDeactivate(account.id);
                                }}>
                                    Пауза
                                </a></li> }
                            <li>
                                <Link
                                    to="/me/my-tasks"
                                    className="btn btn-primary"
                                >
                                    Создать задачу
                                </Link>
                            </li>
                        </ul>
                    </div>
                </td>
            </tr>
          )
    }
}

export default AccountDetail
