import React, { Component } from 'react'

class AccountDetail extends Component {
    render() {
        const { account } = this.props;
        return (
            <ul>
                <li>login: {account.login}</li>
                <li>password: {account.password}</li>
                <li>phone: {account.phone}</li>
                <li>status: {account.status.name}</li>
                <li>proxy: {account.proxy || 'none'}</li>
                <li><a href="#" onClick={(e) => {
                    e.preventDefault();
                    this.props.onDelete(account.id);
                }}>
                    delete
                </a></li>
                {account.status.id === "0" && <li>
                    <a href="#" onClick={(e) => {
                        e.preventDefault();
                        this.props.onActivate(account.id);
                    }}>
                        activate
                    </a></li> }
                {account.status.id !== "0" && <li>
                    <a href="#" onClick={(e) => {
                        e.preventDefault();
                        this.props.onDeactivate(account.id);
                    }}>
                        deactivate
                    </a></li>}
            </ul>)
    }
}

export default AccountDetail