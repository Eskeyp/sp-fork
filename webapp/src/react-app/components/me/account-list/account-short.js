import React, { Component } from 'react'

class AccountShort extends Component {
    render() {
        const { account } = this.props;
        return (
                <ul>
                    <li>login: {account.login}</li>
                    <li>status: {account.status}</li>
                    <li>level: {account.level}</li>
                    <li>proxy: {account.proxy || 'none'}</li>
                </ul>
        )
    }
}

export default AccountShort