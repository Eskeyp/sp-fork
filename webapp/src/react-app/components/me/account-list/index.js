import React, { Component } from 'react'
import { connect } from 'react-redux'
import Loading from '../../common/loading'

import {
    getFullVkAccount,
    deleteVkAccount,
    activateVkAccount,
    deactivateVkAccount,
    createTask,
} from "../../../store/actions/me";
import CreateAccountModal from '../create-account';
import EditAccountModal from '../edit-account';
import Account from './account'
class AccountListPage extends Component {
    constructor(args) {
        super(args);
        this.state = {
            createAccountModalShow: false,
            editAccountModalShow: false,
            editAccountId: null,
        }
    }
    onDelete = (id) => {
        this.props.deleteVkAccount(id);
    };
    onActivate = (id) => {
        this.props.activateVkAccount(id)
    };
    onDeactivate = (id) => {
        this.props.deactivateVkAccount(id)
    };
    createTask = (id) => {
        this.props.createTask(id)
    };
    render() {
        if (this.props.isFetching && this.props.isAccountActivationFetching) {
            return (<div>
                <div className="notifications">выполняется проверка аккаунта, пожалуйста подождите</div>
                <Loading/>
            </div>)
        }
        if (this.props.isFetching) {
            return <Loading/>
        }
        const filterValue = this.props.query && this.props.query['filter']
        const accounts = filterValue !== undefined && filterValue !== null && filterValue !== 'all'
            ? this.props.accounts.filter((acc) =>
                acc.status === Object.keys(this.props.vkAccountsCount)[filterValue])
            : this.props.accounts;
        if (!this.props.accounts || !this.props.accounts.length) {
                return null;
        }
        return ([
          <table className="accounts">
              <thead>
                  <tr>
                      <th>Логин</th>
                      <th>Статус</th>
                      <th>Управление</th>
                  </tr>
              </thead>

              <tbody>
                  {accounts && accounts.map((account, idx) => (
                      <Account
                          idx={idx + 1}
                          account={account}
                          onActivate={this.onActivate}
                          onDeactivate={this.onDeactivate}
                          onDelete={this.onDelete}
                          createTask={this.createTask}
                          setVisibleEditAccountTask={(id) => {
                              this.setState({
                                  editAccountModalShow: true,
                                  editAccountId: id
                              });
                          }}
                      />
                  ))}
              </tbody>
          </table>,
            <button
                className="create_account_new btn btn-success"
                onClick={() => this.setState({createAccountModalShow: true})}
            >
                +
            </button>,
            <CreateAccountModal
                isVisible={this.state.createAccountModalShow}
                setInvisible={() => this.setState({createAccountModalShow: false})}
            />,
            <EditAccountModal
                isVisible={this.state.editAccountModalShow}
                account={(accounts && this.state.editAccountId && accounts.find((acc) => acc.id === this.state.editAccountId)) || null}
                setInvisible={() => this.setState({editAccountModalShow: false})}
            />
        ])
    }
}

const mapStateToProps = (state) => ({
    isAccountActivationFetching: state.me.isAccountActivationFetching,
    accounts: state.me.vkAccounts.list,
    isFetching: state.me.vkAccounts.isFetching,
    vkAccountsCount: state.me.vkAccountsCount,
    query: state.routing.locationBeforeTransitions.query,
})
export default connect(mapStateToProps, {
    deleteVkAccount,
    activateVkAccount,
    deactivateVkAccount,
    createTask,
})(AccountListPage)
