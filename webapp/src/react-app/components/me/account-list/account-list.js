import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import AccountShort from './account-short'
import AccountDetails from './account-detail'
import {
    getFullVkAccount,
    deleteVkAccount,
    activateVkAccount,
    deactivateVkAccount
} from "../../../store/actions/me";

class AccountList extends Component {
    constructor(args) {
        super(args);
        this.state = {
            showDetails: {},
        }
    }
    showDetails = (id) => {
        this.props.getFullVkAccount(id)
    };
    onDelete = (id) => {
        this.props.deleteVkAccount(id);
    };
    onActivate = (id) => {
        this.props.activateVkAccount(id)
    };
    onDeactivate = (id) => {
        this.props.deactivateVkAccount(id)
    };
    render() {
        const { accounts, detailedList } = this.props;
        const { showDetails } = this.state;
        console.log(detailedList);
        return (
            <ul>
                {accounts.map(account => (
                    <li key={account.id}>
                        {!detailedList[account.id] && [
                            <AccountShort account={account} />,
                            <span
                            style={{textDecoration: 'underline', cursor: 'pointer'}}
                            onClick={() => this.showDetails(account.id)}
                            >
                                details
                            </span>
                        ]}
                        {detailedList[account.id] &&
                            <AccountDetails
                                account={detailedList[account.id]}
                                onDelete={this.onDelete}
                                onActivate={this.onActivate}
                                onDeactivate={this.onDeactivate}
                            />}
                    </li>
                ))}
            </ul>
        )
    }
}

AccountList.propTypes = {
    accounts: PropTypes.arrayOf(PropTypes.shape({}))
};
const mapStateToProps = (state) => ({
    detailedList: state.me.vkAccounts.detailedList,
    me: state.me.vkAccounts,
});
export default connect(mapStateToProps, {
    getFullVkAccount,
    deleteVkAccount,
    activateVkAccount,
    deactivateVkAccount,
})(AccountList)