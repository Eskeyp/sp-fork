import React, { Component } from 'react'
import { connect } from 'react-redux'
import CreateAccountForm from '../create-account/create-account-form'
import { updateVkAccount } from '../../../store/actions/me'

class EditAccountPage extends Component {
    onSubmit = () => {
        const {
            account: {
                id,
                login,
                password,
                phone,
                proxy
            }
        } = this.props;
        this.props.updateVkAccount(id, login, password, phone, proxy)
    };
    componentWillReceiveProps(nextProps) {
        if (nextProps.success && !this.props.success) {
            this.props.setInvisible();
        }

    }
    render() {
        const { error, isFetching, isVisible, account } = this.props;
        if (!isVisible) return null;
        if (isVisible) return ([
            <div className="full-page-shadow" />,
            <div className="create-account-modal">
                <div className="create-account-modal__content">
                    {!isFetching &&
                    <CreateAccountForm
                        login={account.login}
                        phone={account.phone}
                        password={account.password}
                        proxy={account.proxy}
                        onSubmit={this.onSubmit}
                        closeForm={this.props.setInvisible}
                        isEditForm={true}
                    />
                    }
                    {isFetching && 'loading...'}
                    {error && <div> {error}</div> }
                </div>
            </div>
        ])
    }
}

const mapStateToProps = (state) => ({
    isFetching: state.me.updateVkAccount.isFetching,
    error: state.me.updateVkAccount.error,
    success: state.me.updateVkAccount.success,
});
export default connect(mapStateToProps, { updateVkAccount })(EditAccountPage)