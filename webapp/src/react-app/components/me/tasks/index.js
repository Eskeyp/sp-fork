import React, { Component } from 'react'
import task from '../../../../static/task_icon.png'
import CreateTaskPage from '../create-task'
import Task from './task'
class Tasks extends Component {
    constructor(args) {
        super(args);
        this.state = {
            isVisibleModal: false,
        }
    }
    render() {
        return (
            <div className="tasks">
                <div className="tasks__content">
                    <Task
                        setVisibleCreateTaskModal={() => this.setState({isVisibleModal: true})}
                    />
                </div>
                <CreateTaskPage
                    isVisible={this.state.isVisibleModal}
                    setInvisibleCreateTaskModal={() => this.setState({isVisibleModal: false})}
                />

            </div>
        )
    }
}
export default Tasks
