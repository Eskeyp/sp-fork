import React, { Component } from 'react'
import task from '../../../../static/task_icon.png'


class Task extends Component {
    render() {
        return (
            <div className="task">
                <div className="task__content">
                <div className="task__title">
                    Раскрутка нового <br/> аккаунта
                </div>
                <div className="task__subtitle">
                    Добавление в друзья пользователей из списка рекомендованных друзей
                </div>
                <div className="task__image">
                    <img src={task} />
                </div>
                <div className="task__create">
                    <a
                        className="button"
                        onClick={(e) => {
                            e.preventDefault();
                            this.props.setVisibleCreateTaskModal();
                        }}
                    >
                        Создать задачу
                    </a>
                </div>
                    </div>
            </div>
        )
    }
}
export default Task