
export function getCookie(name) {
    const matches = document.cookie.match(new RegExp(
        `(?:^|; )${name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1')}=([^;]*)`,
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}
export function setCookie(name, value) {
    const d = new Date();
    d.setTime(d.getTime() + 60 * 60 * 1000);
    document.cookie = `${name}=${encodeURIComponent(value)}; expired=${d}`;
}

export const getToken = (store) => {
    const state = store.getState();

    return state.auth.token || getCookie('sessionid ')
};

export const createGetTokenFunc = (store) => () => {
    const state = store.getState();
    return state.auth.token || getCookie('sessionid')
};

export const createNotNullObject = (obj) => {
    const keys = Object.keys(obj);
    return keys.reduce((acc, curr) => {
        if (obj[curr] !== undefined && obj[curr] !== null && obj[curr] !== '') {
            return {...acc, [curr]: obj[curr]}
        }
        return acc
    }, {})
}