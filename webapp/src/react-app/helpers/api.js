import fetch from "isomorphic-fetch";
import { getTokenFromStore } from '../'
export function put(url, data) {
    return fetch(url, {
        headers: {
            'content-type': 'application/json',
        },
        method: 'PUT',
        credentials: 'same-origin',
        body: JSON.stringify(data),
    })
        .then(response => {
            if (!response.ok) {
                throw response.json()
            }
            if (response.body) {
                return response.json()
            }
            return true;
        })
}export function post(url, data) {
    return fetch(url, {
        headers: {
            'content-type': 'application/json',
        },
        method: 'POST',
        credentials: 'same-origin',
        body: JSON.stringify(data),
    })
        .then(response => {
            if (!response.ok) {
                throw response.json()
            }
            if (response.body) {
                return response.json()
            }
            return true;
        })
}

export function get(url) {
    return fetch(url, {
        headers: {
            'content-type': 'application/json',
        },
        method: 'GET',
        credentials: 'same-origin',
    })
        .then(response => {
            if (!response.ok) {
                throw 'Unexpected server error'
            }
            if (response.body) {
                return response.json()
            }
            return true;
        })
}

export function deleteRequest(url) {
    return fetch(url, {
        headers: {
            'content-type': 'application/json',
        },
        method: 'DELETE',
        credentials: 'same-origin',
    })
        .then(response => {
            if (!response.ok) {
                throw 'Unexpected server error'
            }
            return true;
        })}