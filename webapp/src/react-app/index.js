import React from 'react';
import { render } from 'react-dom';
import {
    Router,
    Route,
    IndexRoute,
    browserHistory
} from 'react-router';
import { Provider } from 'react-redux';
import { syncHistoryWithStore } from 'react-router-redux'
import store from './store';
import { getToken, createGetTokenFunc} from "./helpers";
import Root from './components/root';
import Login from './components/login';
import Register from './components/register';
import RegisterSuccess from './components/register/success'
import Me from './components/me'
import AccountList from './components/me/account-list'
import Tasks from './components/me/tasks'
import TasksList from './components/me/tasks-list'
import style from '../styles/main.scss'
import fontawesome from '@fortawesome/fontawesome'
const history = syncHistoryWithStore(browserHistory, store);

const checkTokenExisting = () => {
      window.location.replace('/welcome/');

  // window.location.replace('/welcome/');
    // if (getToken(store)) {
    //     return history.push('/me')
    // }
    // history.push('/login')
};
export const getTokenFromStore = createGetTokenFunc(store);
render(
    <Provider store={store}>
        <Router history={history}>
            <Route path="/" component={Root} onEnter={checkTokenExisting}/>
            <Route path="/login" component={Login} />
            <Route path="/register" component={Register} />
            <Route path="/register/email" component={RegisterSuccess} />
            <Route component={Me}>
                <Route path="/me/accounts" component={AccountList} />
                <Route path="/me/tasks" component={Tasks} />
                <Route path="/me/my-tasks" component={TasksList} />
            </Route>
        </Router>
    </Provider>,
    document.getElementById('app')
);
